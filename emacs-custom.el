(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-ispell-fuzzy-limit 2)
 '(ac-ispell-requires 3)
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#0a0814" "#f2241f" "#67b11d" "#b1951d" "#4f97d7" "#a31db1" "#28def0" "#b2b2b2"])
 '(ansi-term-color-vector
   [unspecified "#1F1611" "#660000" "#144212" "#EFC232" "#5798AE" "#BE73FD" "#93C1BC" "#E6E1DC"] t)
 '(async-shell-command-buffer (quote new-buffer))
 '(battery-mode-line-format " %p")
 '(beacon-blink-when-focused t)
 '(beacon-color "light goldenrod")
 '(beacon-mode t)
 '(bibtex-completion-fallback-options
   (quote
    (("Google Scholar" . "http://scholar.google.com/scholar?q=%s")
     ("Pubmed" . "http://www.ncbi.nlm.nih.gov/pubmed/?term=%s")
     ("arXiv" . helm-bibtex-arxiv)
     ("Bodleian Library" . "http://solo.bodleian.ox.ac.uk/primo_library/libweb/action/search.do?vl(freeText0)=%s&fn=search&tab=all")
     ("Library of Congress" . "http://www.loc.gov/search/?q=%s&all=true&st=list")
     ("Deutsche Nationalbibliothek" . "https://portal.dnb.de/opac.htm?query=%s")
     ("British National Library" . "http://explore.bl.uk/primo_library/libweb/action/search.do?&vl(freeText0)=%s&fn=search")
     ("Bibliothèque nationale de France" . "http://catalogue.bnf.fr/servlet/RechercheEquation?host=catalogue?historique1=Recherche+par+mots+de+la+notice&niveau1=1&url1=/jsp/recherchemots_simple.jsp?host=catalogue&maxNiveau=1&categorieRecherche=RechercheMotsSimple&NomPageJSP=/jsp/recherchemots_simple.jsp?host=catalogue&RechercheMotsSimpleAsauvegarder=0&ecranRechercheMot=/jsp/recherchemots_simple.jsp&resultatsParPage=20&x=40&y=22&nbElementsHDJ=6&nbElementsRDJ=7&nbElementsRCL=12&FondsNumerise=M&CollectionHautdejardin=TVXZROM&HDJ_DAV=R&HDJ_D2=V&HDJ_D1=T&HDJ_D3=X&HDJ_D4=Z&HDJ_SRB=O&CollectionRezdejardin=UWY1SPQM&RDJ_DAV=S&RDJ_D2=W&RDJ_D1=U&RDJ_D3=Y&RDJ_D4=1&RDJ_SRB=P&RDJ_RLR=Q&RICHELIEU_AUTRE=ABCDEEGIKLJ&RCL_D1=A&RCL_D2=K&RCL_D3=D&RCL_D4=E&RCL_D5=E&RCL_D6=C&RCL_D7=B&RCL_D8=J&RCL_D9=G&RCL_D10=I&RCL_D11=L&ARSENAL=H&LivrePeriodique=IP&partitions=C&images_fixes=F&son=S&images_animees=N&Disquette_cederoms=E&multimedia=M&cartes_plans=D&manuscrits=BT&monnaies_medailles_objets=JO&salle_spectacle=V&Monographie_TN=M&Periodique_TN=S&Recueil_TN=R&CollectionEditorial_TN=C&Ensemble_TN=E&Spectacle_TN=A&NoticeB=%s")
     ("Gallica Bibliothèque Numérique" . "http://gallica.bnf.fr/Search?q=%s"))))
 '(bmkp-last-as-first-bookmark-file "/home/torysa/.emacs.d/bookmarks")
 '(bookmark-save-flag 0)
 '(cider-default-cljs-repl (quote figwheel))
 '(cider-font-lock-dynamically (quote (macro function var core)))
 '(cider-repl-use-pretty-printing t)
 '(command-log-mode-key-binding-open-log [C-f11])
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes t)
 '(debug-on-error nil)
 '(default-input-method "TeX")
 '(delete-old-versions t)
 '(delete-selection-mode nil)
 '(diff-command "wdiff")
 '(diff-switches "")
 '(display-battery-mode nil)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(electric-pair-mode nil)
 '(erc-fill-column 9999)
 '(erc-fill-mode nil)
 '(fancy-battery-mode t)
 '(fancy-battery-show-percentage t)
 '(fci-rule-character-color "#452E2E")
 '(fci-rule-color "#373b41")
 '(fill-column 9999)
 '(global-auto-revert-mode t)
 '(global-hl-line-mode nil)
 '(helm-bibtex-fallback-options
   (quote
    (("Google Scholar" . "http://scholar.google.com/scholar?q=%s")
     ("Pubmed" . "http://www.ncbi.nlm.nih.gov/pubmed/?term=%s")
     ("arXiv" . helm-bibtex-arxiv)
     ("Bodleian Library" . "http://solo.bodleian.ox.ac.uk/primo_library/libweb/action/search.do?vl(freeText0)=%s&fn=search&tab=all")
     ("Library of Congress" . "http://www.loc.gov/search/?q=%s&all=true&st=list")
     ("Deutsche Nationalbibliothek" . "https://portal.dnb.de/opac.htm?query=%s")
     ("British National Library" . "http://explore.bl.uk/primo_library/libweb/action/search.do?&vl(freeText0)=%s&fn=search")
     ("Bibliothèque nationale de France" . "http://catalogue.bnf.fr/servlet/RechercheEquation?host=catalogue?historique1=Recherche+par+mots+de+la+notice&niveau1=1&url1=/jsp/recherchemots_simple.jsp?host=catalogue&maxNiveau=1&categorieRecherche=RechercheMotsSimple&NomPageJSP=/jsp/recherchemots_simple.jsp?host=catalogue&RechercheMotsSimpleAsauvegarder=0&ecranRechercheMot=/jsp/recherchemots_simple.jsp&resultatsParPage=20&x=40&y=22&nbElementsHDJ=6&nbElementsRDJ=7&nbElementsRCL=12&FondsNumerise=M&CollectionHautdejardin=TVXZROM&HDJ_DAV=R&HDJ_D2=V&HDJ_D1=T&HDJ_D3=X&HDJ_D4=Z&HDJ_SRB=O&CollectionRezdejardin=UWY1SPQM&RDJ_DAV=S&RDJ_D2=W&RDJ_D1=U&RDJ_D3=Y&RDJ_D4=1&RDJ_SRB=P&RDJ_RLR=Q&RICHELIEU_AUTRE=ABCDEEGIKLJ&RCL_D1=A&RCL_D2=K&RCL_D3=D&RCL_D4=E&RCL_D5=E&RCL_D6=C&RCL_D7=B&RCL_D8=J&RCL_D9=G&RCL_D10=I&RCL_D11=L&ARSENAL=H&LivrePeriodique=IP&partitions=C&images_fixes=F&son=S&images_animees=N&Disquette_cederoms=E&multimedia=M&cartes_plans=D&manuscrits=BT&monnaies_medailles_objets=JO&salle_spectacle=V&Monographie_TN=M&Periodique_TN=S&Recueil_TN=R&CollectionEditorial_TN=C&Ensemble_TN=E&Spectacle_TN=A&NoticeB=%s")
     ("Gallica Bibliothèque Numérique" . "http://gallica.bnf.fr/Search?q=%s"))))
 '(helm-external-programs-associations
   (quote
    (("pptx" . "loffice")
     ("htm" . "firefox")
     ("csv" . "loffice")
     ("mp4" . "dragon")
     ("odt" . "loffice")
     ("xcf" . "gimp")
     ("html" . "firefox"))))
 '(helm-follow-mode-persistent t)
 '(helm-mode t)
 '(helm-source-names-using-follow (quote ("Org Headings" "Grep" "Occur")))
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(hl-paren-colors
   (quote
    ("#B9F" "#B8D" "#B7B" "#B69" "#B57" "#B45" "#B33" "#B11")))
 '(hl-sexp-background-color "#efebe9")
 '(hscroll-margin 30)
 '(ibuffer-saved-filter-groups
   (quote
    ((""
      ("shell"
       (used-mode . shell-mode))
      ("ssh"
       (filename . "ssh"))
      ("temp"
       (name . "*"))
      ("erc"
       (used-mode . erc-mode)))
     ("work"
      ("shell"
       (used-mode . shell-mode))
      ("ssh"
       (filename . "ssh"))
      ("temp"
       (name . "*"))
      ("erc"
       (used-mode . erc-mode))))))
 '(ibuffer-saved-filters
   (quote
    (("work"
      ((filename . "sudo\\|ssh\\|scp")))
     ("Filter: Narrative"
      ((or
	(filename . "Thesis")
	(filename . "Story"))))
     ("Temp Buffers"
      ((name . "*")))
     ("Thesis"
      ((filename . "Thesis")))
     ("java filter"
      ((mode . java-mode)))
     ("gnus"
      ((or
	(mode . message-mode)
	(mode . mail-mode)
	(mode . gnus-group-mode)
	(mode . gnus-summary-mode)
	(mode . gnus-article-mode)))))))
 '(inhibit-startup-screen t)
 '(initial-scratch-message "")
 '(line-number-mode t)
 '(mark-even-if-inactive t)
 '(max-lisp-eval-depth 9000)
 '(menu-bar-mode nil)
 '(menu-prompting nil)
 '(message-user-organization "Linux Private Site")
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(org-agenda-default-appointment-duration 55)
 '(org-agenda-diary-file "/home/torysa/emacs/org/agenda.org")
 '(org-agenda-files
   (quote
    ("~/emacs/org/phd.org" "~/emacs/org/odh_agenda.org" "~/emacs/org/meals.org" "~/emacs/org/family.org" "~/emacs/org/odh.org" "~/emacs/org/agenda.org" "~/Language/Natural/conversation.org" "~/emacs/org/desiderata.org" "~/emacs/org/td.org")))
 '(org-agenda-skip-deadline-prewarning-if-scheduled t)
 '(org-agenda-span (quote day))
 '(org-agenda-start-with-clockreport-mode t)
 '(org-agenda-start-with-log-mode t)
 '(org-agenda-sticky t)
 '(org-agenda-window-setup (quote current-window))
 '(org-catch-invisible-edits (quote smart))
 '(org-clock-history-length 35)
 '(org-clock-persist t)
 '(org-clock-persist-query-resume nil)
 '(org-clocktable-defaults
   (quote
    (:maxlevel 4 :lang "en" :scope subtree :block nil :wstart 1 :mstart 1 :tstart nil :tend nil :step nil :stepskip0 nil :fileskip0 nil :tags nil :emphasize nil :link t :narrow 40! :indent t :formula nil :timestamp nil :level nil :tcolumns nil :formatter nil)))
 '(org-cycle-include-plain-lists "integrate")
 '(org-default-notes-file "~/org/home/torysa/emacs/org/td.org")
 '(org-directory "~/emacs/org/")
 '(org-email-link-description-format "Email %c: %s")
 '(org-enforce-todo-checkbox-dependencies t)
 '(org-enforce-todo-dependencies t)
 '(org-export-html-postamble nil t)
 '(org-export-with-sub-superscripts (quote {}))
 '(org-export-with-toc nil)
 '(org-from-is-user-regexp "\\<Tory S\\. Anderson\\>")
 '(org-html-postamble nil)
 '(org-html-preamble nil)
 '(org-icalendar-combined-agenda-file "/home/torysa/emacs/org/cal.ics")
 '(org-icalendar-include-todo t)
 '(org-icalendar-timezone "America/Denver")
 '(org-image-actual-width nil)
 '(org-log-note-clock-out t)
 '(org-special-ctrl-o nil)
 '(org-src-fontify-natively t)
 '(org-time-clocksum-format
   (quote
    (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)) t)
 '(org-todo-keywords
   (quote
    ((sequence "TODO(t)" "|" "DONE(d)")
     (sequence "PENDING(p)" "STARTED(s)" "HOLD(h)" "|" "DONE(d)")
     (sequence "|" "CANCELED(c)" "ABSTAINED(a)"))))
 '(org-use-speed-commands t)
 '(orgstruct-heading-prefix-regexp "^;;;.*")
 '(package-selected-packages
   (quote
    (highlight highlight-focus yasnippet undo-tree spacemacs-theme telephone-line rainbow-mode projectile smartparens paradox multiple-cursors iflipb helm-descbinds helm-org-rifle ggtags god-mode exec-path-from-shell ebib dockerfile-mode docker dired-narrow dired-subtree dired-filter crontab-mode col-highlight clean-buffers cider-hydra charmap bookmark+ better-shell beacon counsel-bbdb helm-bbdb bbdb anzu ace-jump-zap ace-isearch ace-jump-mode ace-popup-menu clojure-mode-extra-font-locking skewer-mode skewer which-key smart-mode-line-powerline-theme helm-exwm exwm helm-ext fancy-battery helm-swoop helm-ag helm-org helm-grep helm-config diminish-mode use-package bind-key helm-files clojure-snippets company-quickhelp helm-flyspell diminish markdown-mode ivy delight helm-mu rainbow-identifiers rainbow-delimiters erc-hl-nicks nginx-mode magit pdf-tools ace-window helm-projectile groovy-mode gradle-mode kotlin-mode helm-company company-web ac-html-csswatcher company-auctex company auctex dired+ web-mode php-extras graphviz-dot-mode flymake-php flymake-json flymake-jslint flymake-jshint flymake-css flymake csv-mode anything-exuberant-ctags ac-python 4clojure)))
 '(paradox-automatically-star t)
 '(paradox-github-token "a3942abc214d8fbcbf9574efb1a5ec98ef6a7abf")
 '(password-cache-expiry 3600)
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(recentf-auto-cleanup (quote mode))
 '(recentf-max-menu-items 100)
 '(recentf-max-saved-items 100)
 '(safe-local-variable-values
   (quote
    ((hugo-publish-script . "~/Sites/Blogs/Harmony/publish.sh")
     (hugo-content-dir . "~/Sites/Blogs/Harmony/content/")
     (hugo-publish-script . "~/Sites/Blogs/Desiderata/publish.sh")
     (hugo-content-dir . "~/Sites/Blogs/Desiderata/content/")
     (hugo-publish-script . "~/Sites/Blogs/Tech/publish.sh")
     (hugo-content-dir . "~/Sites/Blogs/Tech/content/")
     (hugo-publish-script . "~/Site/Sites/Blogs/Language/publish.sh")
     (hugo-content-dir . "~/Site/Sites/Blogs/Language/content/")
     (hugo-publish-script . "~/Site/Sites/Blogs/Desiderata/publish.sh")
     (hugo-content-dir . "~/Site/Sites/Blogs/Desiderata/content/")
     (hugo-publish-script . "~/Site/Sites/Blogs/Harmony/publish.sh")
     (hugo-content-dir . "~/Site/Sites/Blogs/Harmony/content/")
     (hugo-publish-script . "~/Site/Sites/Blogs/Tech/publish.sh")
     (hugo-content-dir . "~/Site/Sites/Blogs/Tech/content/")
     (eval require
	   (quote m-buffer-macro)
	   nil t)
     (quote
      (smartrep-mode-line-active-bg
       (solarized-color-blend "#859900" "#073642" 0.2)))
     (quote
      (term-default-bg-color "#002b36"))
     (quote
      (term-default-fg-color "#839496"))
     (quote
      (toggle-enable-multibyte-characters t))
     (quote
      (transient-mark-mode 1))
     (quote
      (undo-tree-visualizer-diff t))
     (quote
      (undo-tree-visualizer-timestamps t))
     (quote
      (vc-annotate-background nil))
     (quote
      (vc-annotate-color-map
       (quote
	((20 . "#cc6666")
	 (40 . "#de935f")
	 (60 . "#f0c674")
	 (80 . "#b5bd68")
	 (100 . "#8abeb7")
	 (120 . "#81a2be")
	 (140 . "#b294bb")
	 (160 . "#cc6666")
	 (180 . "#de935f")
	 (200 . "#f0c674")
	 (220 . "#b5bd68")
	 (240 . "#8abeb7")
	 (260 . "#81a2be")
	 (280 . "#b294bb")
	 (300 . "#cc6666")
	 (320 . "#de935f")
	 (340 . "#f0c674")
	 (360 . "#b5bd68")))))
     (quote
      (vcannotate-very-old-color nil))
     (quote
      (volatile-highlights-mode t))
     (quote
      (weechat-color-list
       (quote
	(unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83"))))
     (quote
      (xterm-color-names
       ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"]))
     (quote
      (xterm-color-names-bright
       ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"])))))
 '(scroll-bar-mode nil)
 '(send-mail-function (quote smtpmail-send-it))
 '(telephone-line-mode t)
 '(tool-bar-mode nil)
 '(vc-follow-symlinks t)
 '(view-read-only t)
 '(visible-bell t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :height 120 :width normal :foundry "outline" :family "DejaVu Sans Mono"))))
 '(beacon-fallback-background ((t (:background "LightGoldenrod1"))))
 '(mode-line-inactive ((t (:background "dim gray" :foreground "#b2b2b2" :box (:line-width 1 :color "#111111" :style pressed-button)))))
 '(org-agenda-date-weekend ((t (:foreground "red" :weight bold))))
 '(popup-scroll-bar-background-face ((t (:background "black"))))
 '(popup-scroll-bar-foreground-face ((t (:background "red"))))
 '(scroll-bar ((t (:background "DarkRed" :foreground "black"))))
 '(vhl/default-face ((t (:foreground "gold" :inverse-video t)))))
