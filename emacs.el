;;; .emacs.el  --- Emacs Init File -S*- lexical-binding: t -*-
;; put .el files here
(add-to-list 'load-path "~/.emacs.d/lisp")

(setq custom-file "~/emacs/emacs-custom.el") ;; to keep this buffer cleaner
(load custom-file)
(setq truncate-partial-width-windows nil) ;; never truncate lines unless I say so

(require 'server) ;; so emacs-client works everywhere
(unless (server-running-p)
  (server-start))

(setq orgstruct-heading-prefix-regexp "^;;;.*")

(require 'package)
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
        ("marmalade" . "http://marmalade-repo.org/packages/")
        ("melpa" . "http://melpa.milkbox.net/packages/")
        ("org" . "http://orgmode.org/elpa/")
        ("melpa-stable" . "http://stable.melpa.org/packages/")))
(package-initialize)

;;; Bootstrap use-package
(eval-when-compile
  (require 'use-package))
;(require 'diminish)                ;; if you use :diminish
(require 'delight)
(require 'bind-key)                ;; if you use any :bind variant

(setq isearch-lax-whitespace nil) ;; set search to look for spaces literally

(load-file "~/.emacs.d/lisp/tsa-misc.el")
(tsa/safe-load-file "~/emacs/local-config.el")

;; store back-up files in a temporary directory
(setq temporary-file-directory "~/.emacs.d/temporary_files")
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; allow upcase and downcase of regions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(setq browse-url-generic-program (executable-find "firefox"))

;; Default pdf file opener (not available in customizer)
(setq browse-pdf-generic-program (executable-find "emacsclient"))

(put 'scroll-left 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)

;; multiple asynch shells
;; (defadvice shell-command (after shell-in-new-buffer (command &optional output-buffer error-buffer))
;;    (when (get-buffer "*Async Shell Command*")
;;      (with-current-buffer "*Async Shell Command*"
;;         (rename-uniquely))))
;; (ad-activate 'shell-command)

(setq-default word-wrap t)

;;;;;;;;;;;;;
;; ;; Keys ;;
;;;;;;;;;;;;;
(global-set-key [C-f1] 'tsa/show-file-name)
(global-set-key [C-f2] 'tsa/insert-previous-message)
(global-set-key [f5] 'toggle-truncate-lines) ;; Linewrap?
(global-set-key [f6] 'global-hl-line-mode) ;; Highlight current line
(global-set-key [f7] 'linum-mode) ;; Line Numbers in margin
;(global-set-key [C-S-f7] 'column-highlight-mode)
(global-set-key [C-f7] 'toggle-scroll-bar) ;; Toggle scroll bar 
(global-set-key [f8] 'tsa/go-or-make-agenda) ;; Check agenda
(global-set-key [C-f8] 'calendar) ;; start calendar
(global-set-key [f9] 'tsa/quick-gnus) ;; Check mail
(global-set-key [f10] 'tsa/go-or-make-bookmark-list) ;; Resume bookmark view
(global-set-key [f11] 'helm-org-capture-templates) ;'org-capture)
(global-set-key [C-f11] 'calculator)
(global-set-key [s-f11] 'toggle-frame-fullscreen)
(global-set-key (kbd "C-x C-d") 'dired) ;; so dired is both C-x C-d and C-x d
(global-set-key (kbd "C-x C-q") 'view-mode) ;; view mode
(global-set-key (kbd "M-C-;") 'comment-box)

;; my URL shortener, using my self-hosted yourls instant
(tsa/safe-load-file "~/.emacs.d/lisp/shorten.el") 
(global-set-key [C-f4] 'shorten-url-at-point)

;; http://bzg.fr/emacs-strip-tease.html

(add-hook 'message-mode-hook 'footnote-mode) ; same footnote key sequence as orgmode
(add-hook 'footnote-mode-hook 
          (lambda ()
            (local-set-key (kbd "C-c C-x f") 'Footnote-add-footnote)))

(defalias 'yes-or-no-p 'y-or-n-p) ; stop asking "yes" http://www.emacswiki.org/emacs/YesOrNoP

(use-package ace-window
  :ensure t)

(use-package ace-popup-menu
  :ensure t
  :config
  (ace-popup-menu-mode 1))

(use-package ace-jump-mode
  :ensure t
  :bind (("C-c SPC" . ace-jump-mode))
  :config
  (setq ace-jump-mode-case-fold nil  ;; case sensitive
        ace-isearch-use-function-from-isearch nil)
  (global-ace-isearch-mode +1)
  (setq ace-jump-mode-submode-list '(ace-jump-line-mode ace-jump-char-mode ace-jump-word-mode) ;; complementary to ace-isearch
        ace-jump-mode-scope 'frame))

(use-package ace-isearch
  :ensure t
  :demand t
  :delight ace-isearch-mode
  :config
  (setq  ace-isearch-input-idle-delay 0.2
         ace-isearch-input-length 9
         ace-isearch-use-ace-jump (quote printing-char)
         ace-isearch-function 'ace-jump-word-mode
                                        ;ace-isearch-function 'avy-goto-char
         ace-isearch-use-jump (quote printing-char)))
(use-package ace-jump-zap
  :ensure t
  :bind (("M-z" . ace-jump-zap-to-char))
  :config
  (setq ajz/zap-function 'kill-region))
(use-package anzu
  :ensure t
  :delight anzu-mode
  :config (global-anzu-mode 1)
  (setq anzu-minimum-input-length 4))

;; (use-package graphviz-dot-mode)

;; bbdb for managing contacts
(use-package bbdb
  :ensure t
  :config
  (setq bbdb-file "~/emacs/bbdb")
  (use-package helm-bbdb :ensure t)
  (use-package counsel-bbdb :ensure t)
  (bbdb-initialize 'gnus 'message)
  (setq bbdb-complete-mail-allow-cycling t
        bbdb-completion-display-record nil))

;; Highlighting cursor position on buffer-change
(use-package beacon
  :ensure t
  :config
  (beacon-mode t))

;; Ties in with Hydra binds for shell management
(use-package better-shell
  :ensure t)

;; bookmark+ is one of those must-have packages
(use-package bookmark+
  :ensure t
  :bind (("C-x j j" . helm-filtered-bookmarks))
  :config
  (setq bmkp-default-handlers-for-file-types 
        '(("\\.pdf$" . find-file)
          ("\\.html$" . "firefox")))
  (defadvice bookmark-jump (after bookmark-jump activate)
    (let ((latest (bookmark-get-bookmark bookmark)))
      (setq bookmark-alist (delq latest bookmark-alist))
      (add-to-list 'bookmark-alist latest)))
  (setq bmkp-prompt-for-tags-flag nil
        bookmark-version-control t)) ; helm will need to be loaded

;; charmap sometimes handy for locating obscure characters
;; (use-package charmap
;;   :ensure t)

;; I'm not sure if I use this
;; (use-package cider-hydra
;;   :ensure t
;;   :config
;;   ;(cider-hydra-on)
;;   )

;; TODO (sometimes caused breaking changes)
;; (use-package clj-refactor
;;   :ensure t)


;; for periodically removing over-abundant buffers
(use-package clean-buffers
  :ensure t
  :config
  (setq clean-buffers-useless-buffer-names '("*Buffer List*" "*Backtrace*" "*Apropos*" "*Completions*" "*Help*" "\\.~master~" "\\*vc-dir\\*" "\\*tramp/.+\\*" "\\*vc-git.+\\*" "\\*helm.*" "\\*magit.*")))

(use-package clojure-mode
  :ensure t
  :config
  (defun my-clojure-mode-hook () 
  (highlight-phrase "TODO" 'web-mode-comment-keyword-face) ;; TODO add a correct face that doesn't depend on web-mode
    ;(clj-refactor-mode 1)
    (yas-minor-mode 1) 
    ;(cljr-add-keybindings-with-prefix "C-c C-m")
    )
  (add-hook 'clojure-mode-hook #'my-clojure-mode-hook))

;; better display
(use-package clojure-mode-extra-font-locking
  :requires clojure-mode
  :ensure t)

;; because it's handy
(use-package col-highlight
  :ensure t
  :bind (("M-<f7>" . column-highlight-mode)))

;; important package for autocompletes
(use-package company
  :ensure t
  :delight company-mode "C"
  :bind (("TAB" . company-indent-or-complete-common))
  :defer t
  :config
  (global-company-mode)
  (use-package "ac-html-csswatcher")
  (use-package "company-auctex")
  (use-package "company-web")
  (use-package "helm-company")
  (define-key company-mode-map (kbd "C-:") 'helm-company)
  (define-key company-active-map (kbd "C-:") 'helm-company)
  (setq company-idle-delay 0.3))

(use-package company-quickhelp
  :ensure t
  :ensure pos-tip
  :config
  (company-quickhelp-mode 1)
  (setq company-quickhelp-delay 0.5))

;; handy for editing crontabs
(use-package crontab-mode
  :ensure t)

;; GOLDEN PACKAGE
(use-package dired+
  :ensure t
  :config (tsa/safe-load-file "~/.emacs.d/lisp/tsa-dired.el"))

;; Ibuffer-style filtering and saved filter groups (persistent, unlike narrow)
(use-package dired-filter
  :ensure t)

;; Smarter dealing with subdirectories
(use-package dired-subtree
  :ensure t)

;; Show colors in dired by file type
;;  (use-package dired-rainbow
;;       :ensure t)

;; Live filtering of dired
(use-package dired-narrow
  :ensure t
  :config 
  (bind-keys
   :map dired-mode-map
   ("C-c n" . dired-narrow)))

(use-package docker
  :ensure t
  :delight " D"
  :config
  (docker-global-mode)
  ;; (setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
  ;; (setq exec-path (append exec-path '("/usr/local/bin")))
  ;; ;; Use "docker-machine env box" command to find out your environment variables
  ;; (setenv "DOCKER_TLS_VERIFY" "1")
  ;; (setenv "DOCKER_HOST" "tcp://10.11.12.13:2376")
  ;; (setenv "DOCKER_CERT_PATH" "/Users/foo/.docker/machine/machines/box")
  ;; (setenv "DOCKER_MACHINE_NAME" "box")
  )

(use-package dockerfile-mode
  :ensure t)

(use-package docker-tramp
  :ensure t)

;; GOLDEN PACKAGE for scholarship
(use-package ebib
  :ensure t
  :bind (("C-c e" . ebib))
  :config
  (setq
   ebib-bib-search-dirs (quote ("~/Documents/School/Story_Theory/Bib"))
   ebib-file-associations (quote (("pdf" . "/usr/bin/xdg-open") ("ps" . "gv")))
   ebib-file-search-dirs (quote ("~/Documents/School/Story_Theory/Readings"))
   ebib-index-display-fields (quote ("title"))
   ebib-preload-bib-files (quote ("story.bib"))
   ebib-bib-search-dirs (quote ("~/Documents/School/Story_Theory/Bib"))
   ebib-file-associations (quote (("pdf" . "/usr/bin/xdg-open") ("ps" . "gv")))
   ebib-file-search-dirs (quote ("~/Documents/School/Story_Theory/Readings"))
   ebib-index-display-fields (quote ("title"))
   ebib-preload-bib-files (quote ("story.bib")))
  (global-set-key "\C-ce" 'ebib)
  (global-set-key "\C-cr" 'helm-bibtex))

(use-package epa-file) ; part of emacs, for security in emacs

(use-package erc
  :config
  (use-package erc-hl-nicks :ensure t) ;; highlight names in erc
  (setq erc-server "irc.freenode.net"
        erc-port 6667
        erc-nick "WorldsEndless"
        erc-user-full-name "Tory S. A." ; do I really want this here? 
        erc-prompt-for-password ()
        erc-hide-list '("JOIN" "PART" "QUIT")
        erc-autojoin-channels-alist '(("freenode.net" "#emacs" "#erc" "#gnus" "#clojure" "#clojurescript"))
        erc-fill-column 9999
        erc-fill-mode nil))

;(use-package presentation) ;; doesn't play well with exwm

;; maybe remove
;; (use-package exec-path-from-shell
;;   :ensure t
;;   :config
;;   (when (memq window-system '(mac ns x))
;;   (exec-path-from-shell-initialize))) 

(use-package exwm
  :ensure t
  :init
  (setq mouse-autoselect-window t
        focus-follows-mouse t)
  (use-package exwm-config)
  (require 'exwm-randr)
  (setq exwm-workspace-show-all-buffers t)
  (setq exwm-layout-show-all-buffers t)
  (exwm-enable)
  (tsa/safe-load-file "~/.emacs.d/local-config.el")
  (exwm-randr-enable) ; https://github.com/ch11ng/exwm/wiki
  (require 'exwm-systemtray)
  (exwm-systemtray-enable)
  :config
  (add-hook 'exwm-update-class-hook
	    (lambda ()  (unless (or (string-prefix-p "sun-awt-X11-" exwm-instance-name)  (string= "gimp" exwm-instance-name))
			  (exwm-workspace-rename-buffer exwm-class-name))))
  (defun tsa/exwm-rename ()
    (interactive)
    (exwm-workspace-rename-buffer (concat exwm-class-name " : " (substring exwm-title 0 (min 12 (length exwm-title))))))
  (add-hook 'exwm-update-title-hook 'tsa/exwm-rename)
  (setq tsa/default-simulation-keys
	'(
	  ;; move
	  ([?\C-b] . left)
	  ([?\M-b] . C-left)
	  ([?\C-f] . right)
	  ([?\M-f] . C-right)
	  ([?\C-p] . up)
	  ([?\C-n] . down)
	  ([?\M-<] . C-home)
	  ([?\M->] . C-end)
	  ([?\C-a] . home) 
	  ([?\C-e] . end) ;; in spreadsheets: C-e C-p - go to top;  C-e C-n - go to bottom
	  ([?\M-v] . prior)
	  ([?\C-v] . next)
	  ;; delete
	  ([?\C-d] . delete)
	  ([?\C-k] . (S-end delete))
	  ([?\M-d] . (C-S-right delete))
	  ;; cut/copy/paste.
					;([?\C-w] . ?\C-x)
	  ([?\M-w] . ?\C-c)
	  ([?\C-y] . ?\C-v)
	  ;; search
	  ([?\C-s] . ?\C-f)
	  ))
  (exwm-input-set-simulation-keys tsa/default-simulation-keys)
  (exwm-input-set-key (kbd "s-r") 'exwm-reset)
  (exwm-input-set-key (kbd "s-n") 'tsa/exwm-rename)
  (exwm-input-set-key (kbd "s-N") 'rename-buffer)
  (exwm-input-set-key (kbd "s-w") #'exwm-workspace-switch)
  (exwm-input-set-key (kbd "s-f")
		      (lambda ()
			(interactive)
			(start-process-shell-command "firefox" nil "firefox")))
  (exwm-input-set-key (kbd "s-t")
		      (lambda ()
			(interactive)
			(start-process-shell-command "Telegram" nil "Telegram")))
  (exwm-input-set-key (kbd "s-<f7>") (lambda () (interactive) (shell-command (executable-find "touchpad_toggle"))))
  (exwm-input-set-key (kbd "C-c o") 'hydra-global-org/body)
  (exwm-input-set-key (kbd "C-M-o") 'hydra-window/body)
  (exwm-input-set-key (kbd "s-l") (lambda () (interactive) (start-process-shell-command "lockscreen" nil "lockscreen")))
  (exwm-input-set-key (kbd "s-g") (lambda () (interactive) (async-shell-command (executable-find "gwenview"))))
  (exwm-input-set-key (kbd "<f8>") 'tsa/go-or-make-agenda)
  (exwm-input-set-key (kbd "<f11>") 'helm-org-capture-templates)
  (exwm-input-set-key (kbd "<f9>") 'tsa/quick-gnus)
  (exwm-input-set-key (kbd "<XF86AudioLowerVolume>") (lambda () (interactive) (shell-command "amixer set Master 2%-")))
  (exwm-input-set-key (kbd "<XF86AudioRaiseVolume>") (lambda () (interactive) (shell-command "amixer set Master 2%+")))
  (exwm-input-set-key (kbd "<XF86AudioMute>") (lambda () (interactive) (shell-command "amixer set Master 1+ toggle")))
  (exwm-input-set-key (kbd "<XF86MonBrightnessDown>") (lambda () (interactive) (shell-command "light -U 5; light")))
  (exwm-input-set-key (kbd "<XF86MonBrightnessUp>") (lambda () (interactive) (shell-command "light -A 5; light")))
  (exwm-input-set-key (kbd "<print>") (lambda () (interactive) (start-process-shell-command "spectacle" nil "spectacle")))
  (exwm-input-set-key (kbd "s-<f9>") (lambda () (interactive) (shell-command "~/bin/get-mail &" nil nil)))
  (exwm-input-set-key (kbd "M-<tab>") 'iflipb-next-buffer)
  (exwm-input-set-key (kbd "s-<tab>") 'ace-window)
  (exwm-input-set-key (kbd "M-<iso-lefttab>") 'iflipb-previous-buffer)
  (use-package helm-exwm
    :ensure t
    :config
    (exwm-input-set-key (kbd "s-b") 'helm-exwm)))

;; good on the laptop
(use-package fancy-battery
  :ensure t
  :config
  (fancy-battery-mode))

(use-package ediff)

;; Statistics and R (I don't always need this)
;; (use-package ess
;;   ;; :ensure t
;;   )

;; GOLD PACKAGE (obvoiusly, as I use it for my email)
(use-package gnus
  :ensure t
  :demand t
  :bind (:map gnus-summary-mode-map
              ("E" . gnus-summary-edit-article)
              ("e" . gnus-summary-put-mark-as-expirable-next)
              ("p" . gnus-summary-prev-article)
              ("P" . gnus-summary-prev-unread-article)
              ("n" . gnus-summary-next-article)
              ("N" . gnus-summary-next-unread-article))
  :config
  (tsa/safe-load-file "~/.gnus")
  ;; Message delaying
  (gnus-delay-initialize)
  (setq message-draft-headers '(References From))
  (gnus-demon-add-handler 'gnus-delay-send-queue 1 nil)
  (gnus-demon-init)
  ;; end message delaying

  (add-to-list 'display-buffer-alist '("*Async Shell Command*" display-buffer-no-window))
  (setq gnus-secondary-select-methods
        '((nnml ""
                (nnimap-address "localhost")
                (nnir-search-engine notmuch)))) ;; notmuch makes gnus much faster for searching
  (setq nnir-method-default-engines '((nnml . notmuch)
                                      (nnimap . imap)
                                      (nntp . gmane)))
  (with-eval-after-load 'gnus-topic
    (define-key gnus-topic-mode-map [remap gnus-topic-indent] 'gnus-topic-select-group)))

;; good package to make a file reading mode much nicer
(use-package god-mode
  :ensure t
  :config
  (global-set-key (kbd "<escape>") 'god-mode-all)
  (setq god-exempt-major-modes nil
        god-exempt-predicates nil)
  (define-key god-local-mode-map (kbd ".") 'repeat)
  (require 'god-mode-isearch)
  (define-key isearch-mode-map (kbd "<escape>") 'god-mode-isearch-activate)
  (define-key god-mode-isearch-map (kbd "<escape>") 'god-mode-isearch-disable)
  (define-key god-local-mode-map (kbd "<backspace>") 'scroll-down-command)
  (defun my-update-cursor ()
    (setq cursor-type (if (or god-local-mode buffer-read-only)
                          'box
                        'bar)))
  (add-hook 'god-mode-enabled-hook 'my-update-cursor)
  (add-hook 'god-mode-disabled-hook 'my-update-cursor))

;; not sure I use this
;; (use-package ggtags
;;   :ensure t)

;; GOLD PACKAGE
(use-package helm
  :ensure t
  :bind (("C-c h" . helm-command-prefix)
         ("M-y" . helm-show-kill-ring)
         ("C-x C-f" . helm-find-files)
         ("C-x b" . helm-mini)
         ("C-h z" . helm-resume)
         ("M-x" . helm-M-x))
  :delight helm-mode
  :config
  (use-package helm-config)  
  (use-package helm-files)
  (use-package helm-grep)
  (use-package helm-org)
  (use-package helm-org-rifle :ensure t)
  (use-package helm-descbinds :ensure t) ;; great way to find keys
  (use-package helm-ag :ensure t)

  ;; spelling-correction done right
  (use-package helm-flyspell :ensure flyspell
    :config
    (defun helm-flyspell-next-error ()
        (interactive)
        (flyspell-goto-next-error)
        (helm-flyspell-correct))
    (global-set-key (kbd "S-<f12>") 'flyspell-buffer)
    (global-set-key (kbd "<f12>") 'helm-flyspell-next-error))
  (helm-descbinds-mode)
  (global-set-key (kbd "C-h k") 'helm-descbinds)
  (global-unset-key (kbd "C-x c"))
  
  (global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
  (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z
  (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
  (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
  (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)
  
  (when (executable-find "curl")
    (setq helm-google-suggest-use-curl-p t))
  (setq helm-quick-update                     t ; do not display invisible candidates
        helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
        helm-buffers-fuzzy-matching           t ; fuzzy matching buffer names when non--nil
        helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
        helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
        helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
        helm-ff-file-name-history-use-recentf t
        helm-semantic-fuzzy-match         t ; imenu fuzzy match
        helm-imenu-fuzzy-match            t)
  (helm-mode 1))

;; (eval-after-load "winner"
;;   '(progn 
;;      (add-to-list 'winner-boring-buffers "*helm M-x*")
;;      (add-to-list 'winner-boring-buffers "*helm mini*")
;;      (add-to-list 'winner-boring-buffers "*Helm Completions*")
;;      (add-to-list 'winner-boring-buffers "*Helm Find Files*")
;;      (add-to-list 'winner-boring-buffers "*helm mu*")
;;      (add-to-list 'winner-boring-buffers "*helm mu contacts*")
;;      (add-to-list 'winner-boring-buffers "*helm-mode-describe-variable*")
;;      (add-to-list 'winner-boring-buffers "*helm-mode-describe-function*")))

;; GOLDEN PACKAGE for scholarship
(use-package helm-bibtex
  :bind (("C-c r" . helm-bibtex))
  :config
  (setq helm-bibtex-bibliography "~/Documents/School/Story_Theory/Bib/story.bib" ; can also accept a list
        helm-bibtex-library-path "~/Documents/School/Story_Theory/Readings"
        helm-bibtex-notes-path "~/Documents/School/Story_Theory/Bib/Annotations"
        helm-bibtex-pdf-open-function 'find-file))

;; (use-package ac-helm
;;   :bind (("C-:" . ac-complete-with-helm))
;;   :config (define-key ac-complete-mode-map (kbd "C-:") 'ac-complete-with-helm))

(use-package helm-swoop
  :ensure t
  :bind (("M-i" . helm-swoop)
         ("M-I" . helm-swoop-back-to-last-point)
         ("C-c M-i" . helm-multi-swoop)
         ("C-x M-i" . helm-multi-swoop-all))
  :config
  (define-key isearch-mode-map (kbd "M-i") 'helm-occur-from-isearch)
  ;; (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)
  ;; (define-key helm-swoop-map (kbd "C-r") 'helm-previous-line)
  ;; (define-key helm-swoop-map (kbd "C-s") 'helm-next-line)
  (define-key helm-multi-swoop-map (kbd "C-r") 'helm-previous-line)
  (define-key helm-multi-swoop-map (kbd "C-s") 'helm-next-line)
  ;; utilize the previous helm pattern
  (setq helm-swoop-pre-input-function
      (lambda () (if (boundp 'helm-swoop-pattern)
                     helm-swoop-pattern "")))
  ;; Save buffer when helm-multi-swoop-edit complete
  (setq helm-multi-swoop-edit-save t)
  
  ;; If this value is t, split window inside the current window
  (setq helm-swoop-split-with-multiple-windows nil)
  
  ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
  (setq helm-swoop-split-direction 'split-window-vertically)
  
  ;; If nil, you can slightly boost invoke speed in exchange for text color
  (setq helm-swoop-speed-or-color t)
  
  ;; ;; Go to the opposite side of line from the end or beginning of line
  (setq helm-swoop-move-to-line-cycle t)
  
  ;; Optional face for line numbers
  ;; Face name is `helm-swoop-line-number-face`
  (setq helm-swoop-use-line-number-face t))

;; GOLDN PACKAGE
(use-package hydra
  :ensure t
  :config (tsa/safe-load-file "~/.emacs.d/lisp/tsa-hydra.el"))

;; GOLDEN PACKAGE
(use-package ibuffer
  :bind (("C-x C-b" . ibuffer))
  :config
  (autoload 'ibuffer "ibuffer" "List buffers." t)

  ;; (defun tsa/pwd-or-matches-filename ()
  ;;   (let ((pwd-without-preamble (substring (pwd) 10)))
  ;;     (if (and (buffer-file-name)
  ;;     (string-match-p pwd-without-preamble (expand-file-name (buffer-file-name))))
  ;;         "``"
  ;;       pwd-without-preamble)))

  ;; (define-ibuffer-column working-directory
  ;;   (:name "WD" :inline t)
  ;;   (tsa/pwd-or-matches-filename))

  ;; (define-ibuffer-sorter working-directory
  ;;     "Sort the buffers by their WD column"
  ;;     (:description "Working Directory")
  ;;     (string-lessp 
  ;;      (with-current-buffer (car a)
  ;;        (tsa/pwd-or-matches-filename))
  ;;      (with-current-buffer (car b)
  ;;        (tsa/pwd-or-matches-filename))))

  ;; (define-key ibuffer-mode-map (kbd "s w") 'ibuffer-do-sort-by-working-directory)

  ;; (define-ibuffer-filter working-directory
  ;;     "Filter by working-directory"
  ;;   (:description "working-directory"
  ;;                 :reader (read-from-minibuffer "Filter by WD Name (regexp): "))
  ;;   (with-current-buffer buf
  ;;     (string-match qualifier (tsa/pwd-or-matches-filename))))
  ;;(define-key ibuffer-mode-map (kbd "/ w") 'ibuffer-filter-by-working-directory)

  (setq ibuffer-formats
        '((mark modified read-only " "
               (name 18 18 :left :elide)
               " "
               (size 9 -1 :right)
               " "
               (mode 16 16 :left :elide)
               " " (working-directory 15 15 :left :elide)
               " " (filename-and-process 0 -1 :right))
         (mark " "
               (name 16 -1)
               " " filename
               "::" working-directory))))

;; alt+tab in exwm
(use-package iflipb
  :ensure t)

(use-package ivy
  :ensure t)

;; spellchecking
(use-package ispell
  :config
  (setq ispell-program-name "hunspell"
        ispell-dictionary "en_US"
        ispell-alternate-dictionary "en_US"
        ispell-complete-word-dict "~/.hunspell_en_US"))

;; Famous for emacs Latex
(use-package latex
  :config
  (setq TeX-PDF-mode t
        TeX-global-PDF-mode t
        TeX-auto-save t
        TeX-parse-self t
        TeX-view-program-list '(("Okular" "okular %o")
                                ("Emacs" "emacsclient %o"))
        TeX-view-program-selection '((output-pdf "Emacs"))
        LaTeX-command-style '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)")))
  (setq-default TeX-master t) ; All master files called "master".
                                        ;(setq TeX-master 'shared) ; shared LaTeX: ask for master file
  (defadvice ispell-send-string (before kill-quotes activate)
    (setq string (replace-regexp-in-string "''" "  " string))))

;; GOLDEN PAKAGE
(use-package magit
  :ensure t
  :config
  (global-magit-file-mode)
  (global-set-key "\C-xg" 'magit-status)
  (setq magit-diff-use-overlays nil))

(use-package markdown-mode
  :ensure t
  :config
  (global-company-mode)
  (use-package "ac-html-csswatcher")
  (use-package "company-web")
  (use-package "helm-company")
  (define-key company-mode-map (kbd "C-:") 'helm-company)
  (define-key company-active-map (kbd "C-:") 'helm-company)
  (setq company-idle-delay 0.3))

(use-package company-quickhelp
  :ensure pos-tip
  :config
  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode)))

(use-package nginx-mode
  :ensure t)

;; obviously GOLDEN PACKAGE
(use-package org
  :demand t
  :bind (:map org-mode-map
         ("C-c ." . org-time-stamp))
  :delight orgstruct-mode "OS"
  :config
  (tsa/safe-load-file "~/.emacs.d/lisp/tsa-org.el")
  (define-key org-capture-mode-map (kbd "C-c TAB") 'org-set-tags)
  (setq org-drawers (quote ("PROPERTIES" "HIDDEN" "NOTE" "COMMENT" "CODE" "CLOCK" "LOGBOOK"))
        CLOCK_INTO_DRAWER t
        org-directory "~/emacs/org/"
        org-enforce-todo-dependencies t
        org-enforce-todo-checkbox-dependencies t))

;; This package is for updating my blogs
(use-package ox-md ;ox-gfm
  :config
  ;; http://www.holgerschurig.de/en/emacs-blog-from-org-to-hugo/
  (defvar hugo-default-banner "http://images.toryanderson.com/default_images/tsa_0.png"
  "Path to my default banner for my blogs")
  (defvar-local hugo-content-dir "~/Site/Sites/Blogs/"
  "Path to Hugo's content directory")
  (defvar-local hugo-publish-script nil
  "Path to the given blog's publish script, which runs hugo and uploads to the server")

  (defun hugo-ensure-property (property)
  "Make sure that a property exists. If not, it will be created.
  Returns the property name if the property has been created,
  otherwise nil."
  (if (org-entry-get nil property)
  nil
  (progn (org-entry-put nil property "")
  property)))
  
  (defun hugo-ensure-properties ()
  "This ensures that several properties exists. If not, these
  properties will be created in an empty form. In this case, the
  drawer will also be opened and the cursor will be positioned
  at the first element that needs to be filled.

  Returns list of properties that still must be filled in"
  (require 'dash)
  (let ((current-time (format-time-string (org-time-stamp-format t t) (org-current-time)))
  first)
  (save-excursion
  (unless (org-entry-get nil "TITLE")
  (org-entry-put nil "TITLE" (nth 4 (org-heading-components))))
  (setq first (--first it (mapcar #'hugo-ensure-property '("HUGO_TAGS" "HUGO_TOPICS" "HUGO_FILE"))))
  (unless (org-entry-get nil "HUGO_DATE")
  (org-entry-put nil "HUGO_DATE" current-time))
  (unless (org-entry-get nil "HUGO_BANNER")
  (org-entry-put nil "HUGO_BANNER" hugo-default-banner)))
  (when first
  (goto-char (org-entry-beginning-position))
  ;; The following opens the drawer
  (forward-line 1)
  (beginning-of-line 1)
  (when (looking-at org-drawer-regexp)
  (org-flag-drawer nil))
  ;; And now move to the drawer property
  (search-forward (concat ":" first ":"))
  (end-of-line))
  first))
  
  (defun hugo ()
  (interactive)
  (unless (hugo-ensure-properties)
  (let* ((title    (concat "title = \"" (org-entry-get nil "TITLE") "\"\n"))
  (date     (concat "date = \"" (format-time-string "%Y-%m-%d" (apply 'encode-time (org-parse-time-string (org-entry-get nil "HUGO_DATE"))) t) "\"\n"))
  (topics   (concat "topics = [ \"" (mapconcat 'identity (split-string (org-entry-get nil "HUGO_TOPICS") "\\( *, *\\)" t) "\", \"") "\" ]\n"))
  (tags     (concat "tags = [ \"" (mapconcat 'identity (split-string (org-entry-get nil "HUGO_TAGS") "\\( *, *\\)" t) "\", \"") "\" ]\n"))
  (banner (concat "banner = \"" (org-entry-get nil "HUGO_BANNER") "\"\n"))
  (images (concat "images = [ \"" (mapconcat 'identity (split-string (org-entry-get nil "HUGO_BANNER") "\\( *, *\\)" t) "\", \"") "\" ]\n"))
  (fm (concat "+++\n"
  title
  date
  tags
  topics
  banner
  images
  "+++\n\n"))
  (file     (org-entry-get nil "HUGO_FILE"))
  (coding-system-for-write buffer-file-coding-system)
  (backend  'md)
  (blog))
  ;; try to load org-mode/contrib/lisp/ox-gfm.el and use it as backend
  (if (require 'ox-gfm nil t)
  (setq backend 'gfm)
  (require 'ox-md))
  (setq blog (org-export-as backend t))
  ;; Normalize save file path
  (unless (string-match "^[/~]" file)
  (setq file (concat hugo-content-dir file))
  (unless (string-match "\\.md$" file)
  (setq file (concat file ".md")))
  ;; save markdown
  (with-temp-buffer
  (insert fm)
  (insert blog)
  (untabify (point-min) (point-max))
  (write-file file)
  (message "Exported to %s" file))
  (org-todo 'done)
  ))))

  (defun hugo-publish-up ()
  (interactive)
  (if (boundp 'hugo-publish-script)
  (progn
  (shell-command hugo-publish-script nil)
  (bury-buffer "*Shell Command Output*"))
  (message "hugo-publish-script not defined for this buffer")))

  (defun hugo-total ()
  (interactive)
  (hugo)
  (hugo-publish-up))
  ) ;; end ox-md

;; (use-package org-mime
;;   :ensure t
;;   :config
;;   (defun org-mime-org-buffer-htmlize ()
;;     "Create an email buffer containing the current org-mode file
;;   exported to html and encoded in both html and in org formats as
;;   mime alternatives."
;;     (interactive)
;;     (org-mime-send-buffer 'html)
;;     (message-goto-to))
;;   (defun org-mime-compose (body fmt file &optional to subject headers)
;;   (require 'message)
;;   (let ((bhook
;;          (lambda (body fmt)
;;            (let ((hook (intern (concat "org-mime-pre-"
;;                                        (symbol-name fmt)
;;                                        "-hook"))))
;;              (if (> (eval `(length ,hook)) 0)
;;                  (with-temp-buffer
;;                    (insert body)
;;                    (goto-char (point-min))
;;                    (eval `(run-hooks ',hook))
;;                    (buffer-string))
;;                body))))
;;         (fmt (if (symbolp fmt) fmt (intern fmt)))
;;         (files (org-element-map (org-element-parse-buffer) 'link
;;                  (lambda (link)
;;                    (when (string= (org-element-property :type link) "file")
;;                      (file-truename (org-element-property :path link)))))))
;;     (compose-mail to subject headers nil)
;;     (message-goto-body)
;;     (cond
;;      ((eq fmt 'org)
;;       (require 'ox-org)
;;       (insert (org-export-string-as
;;                (org-babel-trim (funcall bhook body 'org)) 'org t)))
;;      ((eq fmt 'ascii)
;;       (require 'ox-ascii)
;;       (insert (org-export-string-as
;;                (concat "#+Title:\n" (funcall bhook body 'ascii)) 'ascii t)))
;;      ((or (eq fmt 'html) (eq fmt 'html-ascii))
;;       (require 'ox-ascii)
;;       (require 'ox-org)
;;       (let* ((org-link-file-path-type 'absolute)
;;              ;; we probably don't want to export a huge style file
;;              (org-export-htmlize-output-type 'inline-css)
;;              (org-html-with-latex 'dvipng)
;;              (html-and-images
;;               (org-mime-replace-images
;;                (org-export-string-as (funcall bhook body 'html) 'html t)))
;;              (images (cdr html-and-images))
;;              (html (org-mime-apply-html-hook (car html-and-images))))
;;         (insert (org-mime-multipart
;;                  (org-export-string-as
;;                   (org-babel-trim
;;                    (funcall bhook body (if (eq fmt 'html) 'org 'ascii)))
;;                   (if (eq fmt 'html) 'org 'ascii) t)
;;                  html)
;;                 (mapconcat 'identity images "\n")))))
;;     (mapc #'mml-attach-file files))))

;; more powerful than I usually know what to do with
(use-package multiple-cursors
  :ensure t)

;; superior package manager
(use-package paradox
  :ensure t
  :config
  (setq paradox-lines-per-entry 3
        paradox-automatically-star t
        paradox-github-token "c621dd25ac15c17cff8c27df68780a090549d457"
	)
  (tsa/safe-load-file "~/.emacs.d/lisp/helm-org-clock.el"))

;; (use-package paredit
;;   :ensure t
;;   :delight paredit-mode "PE"
;;   :config
;;   (add-hook 'emacs-lisp-mode-hook #'paredit-mode)
;;   (add-hook 'clojure-mode-hook #'paredit-mode))

(use-package smartparens-config
  :ensure smartparens
  :demand t
  ;:bind (("C-<f5>" . smartparens-mode)) ; prevents evaluation of the :config stuff
  :config
                                        ;(smartparens-global-mode)
  (show-smartparens-global-mode)
  (sp-use-paredit-bindings)
  (add-hook 'emacs-lisp-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'clojure-mode-hook 'turn-on-smartparens-strict-mode)
  (add-hook 'cider-repl-mode-hook #'turn-on-smartparens-strict-mode)
  (add-hook 'message-mode-hook 'turn-off-smartparens-mode)
  (add-hook 'org-mode-hook 'turn-off-smartparens-mode)
  (bind-keys
   :map smartparens-strict-mode-map
   (";" . sp-comment)
   ("M-f" . sp-forward-symbol)
   ("M-b" . sp-backward-symbol)
   ("M-a" . sp-beginning-of-sexp)
   ("M-e" . sp-end-of-sexp)))
(use-package paren
  :config
  (show-paren-mode 1))

;; GOLDEN PACKAGE
(use-package pdf-tools
  :ensure t
  :demand t
  :bind (:map pdf-view-mode-map
              ("G" . pdf-view-goto-page))
  :config
  (pdf-tools-install)
  (setq pdf-view-continuous t ; don't automatically jump to next page
        pdf-misc-print-programm "/usr/bin/lp"
        pdf-misc-print-programm-args '( ;"-d ODH_Workroom"
                                       "-o media=letter"
                                       "-o sides=two-sided-long-edge"))
  
  (add-hook 'pdf-view-mode-hook 'auto-revert-mode)
  (define-pdf-cache-function pagelabels)
  (defun pdf-view-page-number ()
    (interactive)
    (if (called-interactively-p)
	(message "[pg %s/%s/%s]"
		 (nth (1- (pdf-view-current-page))
		      (pdf-cache-pagelabels))
		 (number-to-string (pdf-view-current-page))
		 (number-to-string (pdf-cache-number-of-pages)))
      (format  "[pg %s/%s/%s]"
		 (nth (1- (pdf-view-current-page))
		      (pdf-cache-pagelabels))
		 (number-to-string (pdf-view-current-page))
		 (number-to-string (pdf-cache-number-of-pages))))))

;; since I no longer use web-mode
(use-package php-mode
  :config
  (use-package ac-php :ensure t))

;; GOLDEN PACKAGE for developers
(use-package projectile
  :ensure t
  :delight '(:eval (concat "[P: " (projectile-project-name) "]"))
  :config
  (use-package helm-projectile :ensure t)
  (projectile-global-mode)
  (setq projectile-completion-system 'helm
        projectile-switch-project-action 'helm-projectile)
  (define-key projectile-command-map (kbd "s g") 'helm-projectile-grep))

;; handy for CSS
(use-package rainbow-mode
  :ensure t)

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package rainbow-identifiers
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'rainbow-identifiers-mode))

(use-package rainbow-mode
  :ensure t)

(use-package recentf
  :ensure t
  :bind (("C-x C-r" . recentf-open-files))
  :config
  (setq recentf-max-menu-items 100)
  (recentf-mode 1))

;; easily my favorite mode-line program so far
(use-package telephone-line
  :ensure t
  :after (pdf-view)
  :config
  (telephone-line-defsegment telephone-line-pdf-segment ()
    (if (eq major-mode 'pdf-view-mode)
  	(propertize (pdf-view-page-number)
                    'face '(:inherit)
                    'display '(raise 0.0)
  		    'mouse-face '(:box 1)
                    'local-map (make-mode-line-mouse-map
                                'mouse-1 (lambda ()
                                           (interactive)
                                           (pdf-view-goto-page))))))
  (setq telephone-line-primary-left-separator 'telephone-line-cubed-left
        telephone-line-secondary-left-separator 'telephone-line-cubed-hollow-left
        telephone-line-primary-right-separator 'telephone-line-cubed-right
        telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right
        telephone-line-height 24
        telephone-line-evil-use-short-tag t)
  (setq telephone-line-lhs '((accent telephone-line-vc-segment telephone-line-erc-modified-channels-segment telephone-line-process-segment telephone-line-pdf-segment)
  			     (nil telephone-line-projectile-segment telephone-line-buffer-segment)))
  (telephone-line-mode t))

;; My primary theme
(use-package spacemacs-common
  :ensure spacemacs-theme
  :config
  (load-theme 'spacemacs-dark t)
  (tsa/face-tweaks))

(use-package sql
  :config
                                        ;(setq sql-mysql-options (list "-P 33600"))
  (setq sql-mysql-login-params (append sql-mysql-login-params '(port)))
  (setq sql-port 3306) ;; default MySQL port
  (setq sql-postgres-login-params
        '((user :default "torysa")
          (database :default "torysa")
          server
          password
          port)))

;; interactive javascript development
(use-package skewer-mode
  :ensure t
  :config
  (skewer-setup))

;; comes with emacs, but still GOLDEN PACKAGE for anyone who works on multiple servers
(use-package tramp
  :config
  (setq tramp-default-method "ssh")
  ;; https://github.com/emacs-helm/helm/issues/981
  (add-to-list 'tramp-default-proxies-alist
               '(nil "\\`root\\'" "/ssh:%h:"))
  (add-to-list 'tramp-default-proxies-alist
               '((regexp-quote (system-name)) nil nil))
  (defun tramp-ibuffer-cleanup ()
    (interactive)
    (tramp-cleanup-all-buffers)
    (ibuffer-update nil))
  (bind-keys
   :map ibuffer-mode-map
   ("G" . tramp-ibuffer-cleanup)))

(use-package undo-tree
  :ensure t
  :delight undo-tree-mode
  :bind (("C-x /" . undo-tree-visualize))
  :config
  (global-undo-tree-mode t)
  )


(use-package frame
  :config
  (setq blink-cursor-blinks -1)
  (blink-cursor-mode))

(use-package shell
  :config ;http://stackoverflow.com/questions/704616/something-wrong-with-emacs-shell
  (autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
  
  (add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
  (add-to-list 'display-buffer-alist
               '("^\\*shell\\*$" . (display-buffer-same-window)))) ;; don't open shell in a new window

(use-package hi-lock
  :config
  (global-hi-lock-mode 1))

;; new one I'm trying, created by the excellent Drew Adams
(use-package highlight
  :ensure t)

;;  (use-package volatile-highlights
;;    :ensure t
;;    :delight volatile-highlights-mode
;;    :config
;;    (volatile-highlights-mode t))

;; nice interactive help on keystrokes
(use-package which-key
  :ensure t
  :config
  (which-key-mode))

;; very handy for navigatoin amidst windows
(use-package windmove
  :ensure t
  :config
  (setq windmove-default-keybindings t)
  (setq max-specpdl-size 10000))

;; for undoing window changes
(use-package winner
  :config
  (add-to-list 'winner-boring-buffers "*helm M-x*")
  (add-to-list 'winner-boring-buffers "*helm mini*")
  (add-to-list 'winner-boring-buffers "*Helm Completions*")
  (add-to-list 'winner-boring-buffers "*Helm Find Files*")
  (add-to-list 'winner-boring-buffers "*helm mu*")
  (add-to-list 'winner-boring-buffers "*helm mu contacts*")
  (add-to-list 'winner-boring-buffers "*helm-mode-describe-variable*")
  (add-to-list 'winner-boring-buffers "*helm-mode-describe-function*")
  (winner-mode 1))

;; probably GOLDEN PACKAGE
(use-package yasnippet
  :delight yas-minor-mode "Y"
  :ensure t
  :config
  (add-to-list 'yas-snippet-dirs "~/emacs/Snippets")
  (use-package clojure-snippets :ensure t)
  (yas-global-mode))

  ;; TODO ERROR ;; moved to end; is it an ordering thing?
(use-package cider
  :ensure t
  :config
  (setq cider-repl-use-clojure-font-lock t
        cider-font-lock-dynamically '(macro core function var))
  (cider-repl-toggle-pretty-printing)
  (setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")
  (defun figwheel-connect ()
    (interactive)
    (cider-connect "localhost" "7002"))
  )

;; finally, head to agenda as our first screen on starting emacs
(tsa/go-or-make-agenda)
