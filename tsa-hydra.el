;;; Usage Functions
(defun hydra-ace-cmd ()
  (interactive)
  (ace-window 1)
  (add-hook 'ace-window-end-once-hook
            'hydra-window/body))

(defun hydra-split-vertical ()
  (interactive)
  (split-window-right)
  (windmove-right))

(defun hydra-split-horizontal ()
  (interactive)
  (split-window-below)
  (windmove-down))

(defun hydra-swap ()
  (interactive)
  (ace-window 4)
  (add-hook 'ace-window-end-once-hook
            'hydra-window/body))

(defun hydra-del-window ()
  (interactive)
  (ace-window 16)
  (add-hook 'ace-window-end-once-hook
            'hydra-window/body))

;;; Hydra settings
(global-set-key
 (kbd "C-;")
 (defhydra hydra-multiplecursors (:color red)
   "MultiCursors"
   ("n" mc/mark-next-like-this "next~")
   ("N" mc/unmark-next-like-this "un next~")
   ("p" mc/mark-previous-like-this "prev~")
   ("P" mc/unmark-previous-like-this "un prev~")
   ("a" mc/mark-all-like-this "all~")
   ("r" mc/mark-all-in-region "all-region")
   ("d" mc/mark-all-dwim "all-dwim")
   ("SPC" nil)
   ))

(global-set-key
 (kbd "C-z")
 (defhydra hydra-shells (:color blue)
   "Shell"
   ("z" better-shell-shell "bettersh")
   ("C-z" better-shell-shell "bettersh")
   ("Z" better-shell-remote-open "better-remote")
   ("e" eshell "eshell")
   ("t" term "term")))

(global-set-key
 (kbd "C-c o")
 (defhydra hydra-global-org (:color blue
				    :hint nil
                                    :body-pre (setq exwm-input-line-mode-passthrough ''t)
                                    :post (setq exwm-input-line-mode-passthrough nil))
   "
Timer^^        ^Clock^         ^Capture^         ^Other^                  ^Hugo^
--------------------------------------------------------------------------------
[_C-t_]tart        _w_ clock in    _c_apture         _,_ Priority       _h_ugo
[_C-s_]top         _o_ clock out   _l_ast capture    _s_ rifle-current  _u_pload
_r_eset            _j_ clock goto                                       _t_otal
_p_rint
[_C-S_]et
"
   ;("t" org-timer-start)
   ("C-t"  org-timer-start)
   ("s" helm-org-rifle-current-buffer)
   ("S" helm-org-rifle)
   ("C-s"  org-timer-stop)
   ("C-S"  org-timer-set-timer)
   ;; Need to be at timer
   ("r" org-timer-set-timer)
   ("C-r"  org-timer-set-timer)
   ;; Print timer value to buffer
   ("p" org-timer)
   ("C-p"  org-timer)
   ("w" (org-clock-in '(4)))
   ("C-w"  (org-clock-in '(4)))
   ("o" org-clock-out)
   ("C-o"  org-clock-out)
   ;; Visit the clocked task from any buffer
   ("j" org-clock-goto)
   ("C-j"  org-clock-goto)
   ("c" helm-org-capture-templates) ;org-capture)
   ("C-c"  helm-org-capture-templates) ;org-capture)
   ("l" org-capture-goto-last-stored)
   ("C-l"  org-capture-goto-last-stored)
   ("," org-priority)
   ("C-,"  org-priority)
   ("h" hugo)
   ("u" hugo-publish-up)
   ("t" hugo-total)))


;;* Windmove helpers
(require 'windmove) ; also already added in my emacs-el
(defun hydra-move-splitter-left (arg)
  "Move window splitter left."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'right))
      (shrink-window-horizontally arg)
    (enlarge-window-horizontally arg)))

(defun hydra-move-splitter-right (arg)
  "Move window splitter right."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'right))
      (enlarge-window-horizontally arg)
    (shrink-window-horizontally arg)))

(defun hydra-move-splitter-up (arg)
  "Move window splitter up."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'up))
      (enlarge-window arg)
    (shrink-window arg)))

(defun hydra-move-splitter-down (arg)
  "Move window splitter down."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'up))
      (shrink-window arg)
    (enlarge-window arg)))


                                        ; Windows
(global-set-key [C-up] 'enlarge-window)
(global-set-key [C-down] (lambda () (interactive)
			   (enlarge-window -1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; The Big Hydra for Ultimate Navigation ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                        ;(global-set-key
(bind-key* "C-M-o"
                                        ; (kbd "C-M-o")
           (defhydra hydra-window (:body-pre (setq exwm-input-line-mode-passthrough ''t)
                                             :post (setq exwm-input-line-mode-passthrough nil))
             "
Movement^^        ^Split^         ^Switch^		^Resize^
----------------------------------------------------------------
_h_ ←       	_v_ertical    	_b_uffer		_q_ X←
_j_ ↓        	_x_ horizontal	_f_ind files	_w_ X↓
_k_ ↑        	_z_ undo      	_a_ce 1		_e_ X↑
_l_ →        	_Z_ redo      	_s_ave		_r_ X→
_F_ollow		_D_lt Other   	_S_wap		max_i_mize
_SPC_ cancel	_o_nly this   	_d_elete	
_,_ Scroll←			_p_roject
_._ Scroll→
"
             ("h" windmove-left )
             ("C-h"  windmove-left )
             ("j" windmove-down )
             ("C-j"  windmove-down )
             ("k" windmove-up )
             ("C-k"  windmove-up )
             ("l" windmove-right )
             ("C-l"  windmove-right )
             ("q" hydra-move-splitter-left)
             ("C-q"  hydra-move-splitter-left)
             ("w" hydra-move-splitter-down)
             ("C-w"  hydra-move-splitter-down)
             ("e" hydra-move-splitter-up)
             ("C-e"  hydra-move-splitter-up)
             ("r" hydra-move-splitter-right)
             ("C-r"  hydra-move-splitter-right)
             ("b" helm-mini)
             ("C-b"  helm-mini)
             ("f" helm-find-files)
             ("C-f"  helm-find-files)
             ("p" helm-projectile)
             ("C-p"  helm-projectile)
             ("F" follow-mode)
             ("C-F"  follow-mode)
             ("a" hydra-ace-cmd)
             ("C-a"  hydra-ace-cmd)
             ("v" hydra-split-vertical)
             ("C-v"  hydra-split-vertical)
             ("x" hydra-split-horizontal)
             ("C-x"  hydra-split-horizontal)
             ("s" hydra-swap)
             ("C-s"  hydra-swap)
             ("S" toggle-window-split)
             ("C-S" toggle-window-split)
             ("d" delete-window)
             ("C-d"  delete-window)
             ("D" hydra-del-window)
             ("C-D"  hydra-del-window)
             ("o" delete-other-windows)
             ("C-o"  delete-other-windows)
             ("i" ace-maximize-window)
             ("C-i"  ace-maximize-window)
             ("z" (progn
                    (winner-undo)
                    (setq this-command 'winner-undo)))
             ("C-z" (progn
                      (winner-undo)
                      (setq this-command 'winner-undo)))
             ("Z" winner-redo)
             ("C-Z"  winner-redo)
             ("SPC" nil)
             ("C-SPC"  nil)
             ("." scroll-left)
             ("," scroll-right)))

(global-set-key
 (kbd "M-g")
 (defhydra hydra-goto ()
   "Go To"
   ("g" goto-line "line") ; reserve for normal M-g g function (may be different in some modes)
   ("M-g" goto-line "line")
   ("TAB" move-to-column "col")
   ("l" ace-jump-line-mode "ace line" :color blue)
   ("c" goto-char "char")
   ("n" next-error "next err")
   ("p" previous-error "prev err")
   ("r" anzu-query-replace "qrep")
   ("R" anzu-query-replace-regexp "rep regex")
   ("t" anzu-query-replace-at-cursor "rep cursor")
   ("T" anzu-query-replace-at-cursor-thing "rep cursor thing")
   ("," scroll-right "scroll leftward")
   ("." scroll-left "scroll rightward")
   ("[" backward-page "back page")
   ("]" forward-page "forward page")
   ("SPC" nil "cancel")
   ))

(defhydra hydra-gnus-group ()
  "Gnus Group"
					;    ("TAB" gnus-topic-indent "indent")
					;    ("<tab>" gnus-topic-indent "indent")
  ("#" gnus-topic-mark-topic "mark")
  ("u" gnus-topic-unmark-topic "unmark")
  ("C" gnus-topic-copy-matching "Copy-m")
  ("D" gnus-topic-remove-group "DLT")
  ("H" gnus-topic-toggle-display-empty-topics "Hide Empty")
  ("M" gnus-topic-move-matching "Move-m")
  ("S" gnus-topic-sort-map "sort")
  ("c" gnus-topic-copy-group "copy")
  ("h" gnus-topic-hide-topic "hide")
  ("j" gnus-topic-jump-to-topic "jump")
  ("m" gnus-topic-move-group "move")
  ("N" gnus-topic-create-topic "new")
  ("n" gnus-topic-goto-next-topic "→")
					;    ("TAB" gnus-topic-goto-next-topic "→")
  ("<tab>" gnus-topic-goto-next-topic "→")
  ("p" gnus-topic-goto-previous-topic "←")
					;    ("BACKTAB" gnus-topic-goto-previous-topic "←")
  ("<backtab>" gnus-topic-goto-previous-topic "←")
  ("r" gnus-topic-rename "rename")
  ("s" gnus-topic-fold-this-topic "show")
  ("DEL" gnus-topic-delete "delete")
  ("SPC" nil "cancel"))

(global-set-key
 (kbd "C-c b")
 (defhydra hydra-bbdb ()
   "Go To"
   ("b" helm-bbdb "BBDB-helm")
   ("B" bbdb "BBDB")
   ("c" bbdb-create "Create")
   ("x" bbdb-search-xfields "X-Field")
   ("s" bbdb-snarf "Snarf")
   ("SPC" nil "cancel")
   ))
