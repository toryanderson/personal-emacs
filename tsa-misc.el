;; Miscellaneous functions
;; Tory S. Anderson (concat "mail" "@" "toryanderson" ".com")

(defun tsa/safe-load-file (file-to-load)
  "Load a file if it exists, otherwise message and move-on. 
   Designed for sharing config files on different systems."
  (if (file-exists-p file-to-load)
      (load file-to-load)
    (message (format "File doesn't exist to load: %s" file-to-load))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TSA scroll functions					  ;;
;; scroll-other-window that respects the mode of the	  ;;
;; other window (ideal for PDFview mode, info mode, etc)  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar tsa/scroll-functions
      '(("default" :down scroll-down :up scroll-up)
	(pdf-view-mode :down pdf-view-scroll-down-or-previous-page :up pdf-view-scroll-up-or-next-page)
	(help-mode :down Info-scroll-down :up Info-scroll-up)
	)
      "The functions that should be used when scrolling other windows of a particular buffer type.
If buffer type is not included, 'default' will be used. Used by tsa/smart-other-scroll")

(defun tsa/smart-other-scroll (dir)
  "Scroll the other window with appropriate function; `dir' should be :up or :down "
  (interactive)
  (let* ((other-buffer-mode (with-current-buffer (window-buffer (other-window-for-scrolling)) major-mode))
	 (fun (or (plist-get (cdr (assoc other-buffer-mode tsa/scroll-functions)) dir)
		  (plist-get (cdr (assoc "default" tsa/scroll-functions)) dir))))
    (if fun
	(with-selected-window (other-window-for-scrolling)
	  (call-interactively fun))
      )))

(defun tsa/other-scroll-up () "use `tsa/smart-other-scroll' :up" (interactive) (tsa/smart-other-scroll :up))
(defun tsa/other-scroll-down () "use `tsa/smart-other-scroll' :down" (interactive) (tsa/smart-other-scroll :down))
(define-key global-map (kbd "C-M-v") 'tsa/other-scroll-up)
(define-key global-map (kbd "C-M-S-v") 'tsa/other-scroll-down)

(defun tsa/go-or-make-bookmark-list ()
  "Visit an existing bookmark list rather than creating a new one"
  (interactive)
  (let ((b "*Bookmark List*")) (if (get-buffer b) (switch-to-buffer b) (bookmark-bmenu-list))))

(defun tsa/scripture-links ()
  "Convert a reference to a scripture into an orgmode-link to the scripture online"
  (interactive)
  (if (region-active-p)
      (replace-regexp "\\(.*[0-9]+?:[0-9]+\\)" "[[https://www.lds.org/scriptures/search?lang=eng&query=\\1&x=0&y=0][\\1]]"
                      nil (region-beginning) (region-end))
     (replace-regexp "^\\(.*[0-9]+?:[0-9]+\\)" "[[https://www.lds.org/scriptures/search?lang=eng&query=\\1&x=0&y=0][\\1]]")
     ))

;;insert time
;; (defun insert-current-time () (interactive)
;;     (insert (shell-command-to-string "echo -n $(date +%H:%M)")))
;; (global-set-key (kbd "C-c C-.") 'insert-current-time)
(defun tsa/insert-current-time (&optional arg) (interactive "P")
       (let ((command-options (cond 
			       ((equal arg '(16)) "date")
			       ((equal arg '(4)) "date +%Y.%m.%d")
			       (t "date +%H:%M"))))
	 (insert (shell-command-to-string (concat "echo -n $(" command-options ")")))))
(global-set-key (kbd "C-c C-.") 'tsa/insert-current-time)

;; (defun tsa/insert-fortune (&optional arg) (interactive "P")
;;        (let ((command-options (if arg arg fortune-file)))
;; 	 (message (shell-command-to-string (concat "fortune $(" command-options ")")))))


;; show file full name/path
(defun tsa/show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message (buffer-file-name)))

;; copy/paste last message result
;; (org-defkey org-agenda-mode-map "\C-c\." 'org-agenda-time-stamp)

;; print last message http://tapoueh.org/blog/2009/08/03-some-emacs-nifties
;; current-message is already lost by the time this gets called
(defun tsa/previous-message (&optional nth)
  "get nth last line of *Message* buffer, or the previous line"
  (with-current-buffer (get-buffer "*Messages*")
    (save-excursion
      (goto-char (point-max))
      (setq nth (if nth nth 1))
      (while (> nth 0)
	(previous-line)
	(setq nth (- nth 1)))
      (buffer-substring (line-beginning-position) (line-end-position)))))

(defun tsa/insert-previous-message (&optional nth)
  "insert last message of *Message* to current position"
  (interactive "p")
  (insert (format "%s" (tsa/previous-message nth))))

(defun tsa/face-tweaks ()
  "Reset the font and mode-line values, esp. after a theme change"
  (interactive)
  ;(load-theme 'smart-mode-line-powerline)
  ;; * fonts
  ;(set-face-attribute 'default nil :font "DejaVu Sans Mono-24")
  (set-face-attribute 'default nil :font "DejaVu Sans Mono-14")
  ;(set-face-attribute 'mode-line nil :font "DejaVu Sans Mono-22")
  (set-face-attribute 'mode-line nil :font "DejaVu Sans Mono-14")
  ;;display-time-mode mail notification
  (defface display-time-mail-face '((t (:background "red")
                                        ;(:foreground "red")
                                       ))
    "If display-time-use-mail-icon is non-nil, its background colour is that
of this face. Should be distinct from mode-line. Note that this does not seem
to affect display-time-mail-string as claimed."))

(defun tsa/load-theme ()
  "Load themes, disabling all other themes"
  (interactive)
  (mapc 'disable-theme custom-enabled-themes)
  (call-interactively 'load-theme)
  ;(smart-mode-line-enable)
  (telephone-line-mode 1)
  ;; (sml/apply-theme 'powerline)
  (tsa/face-tweaks))

(global-set-key (kbd "C-c T") 'tsa/load-theme)

(defun tsa/text-to-initials (beginning end)
  "Reduce the selected alphabetic string to just punctuation and initials, 
   stripping whitespace and non-initial letters"
  (interactive "r")
  (replace-regexp "\\([A-Za-z]\\)[A-Za-z]*\\([-:,;.]\\)* *"
		  "\\1\\2"
		  nil
		  beginning
		  end
		  ))

(require 'fortune)

;; Be sure to set the following to your specific fortune files
(setq fortune-dir "~/.fortunes"
      fortune-file "~/.fortunes/fortunes")

;; http://datko.net/tag/emacs/
(defun fortune-message (&optional file)
  "Display a fortune cookie to the mini-buffer.
If called with a prefix, it has the same behavior as `fortune'.
Optional FILE is a fortune file from which a cookie will be selected."
  (interactive (list (if current-prefix-arg
                         (fortune-ask-file)
                       fortune-file)))
  (message (fortune-generate)))

(defun fortune-generate (&optional file)
  "Generate a new fortune and return it as a string.
Without an optional FILE argument it will use the value of `fortune-file'."
  (save-excursion
    (progn (fortune-in-buffer t file)
           (with-current-buffer fortune-buffer-name
             (buffer-string)))))

;; Convert Sams budget
;;; immediately after importing a sams Csv
(fset 'tsa/sams-budget-conversion ;; Adds two columns, positions them correctly, removes some columns
   [?\C-k ?\C-k M-S-right M-S-right M-right M-right M-right M-right M-right M-right ?\C-a tab tab tab M-S-left tab M-S-right M-right])

(defun tsa/sams-import ()
  (interactive)
  (let ((_ (call-interactively 'org-table-import)))
    (kmacro-call-macro 'tsa/sams-budget-conversion)))

;; (fset 'test (message "3"))
;; (test) ;; eval: Invalid function: test

(defun bbdb-complete-mail (&optional beg cycle-completion-buffer)
  "In a mail buffer, complete the user name or mail before point.
Completion happens up to the preceeding colon, comma, or BEG.
Return non-nil if there is a valid completion, else return nil.

Completion behaviour obeys `bbdb-completion-list' (see there).
If what has been typed matches a unique BBDB record, insert an address
formatted by `bbdb-dwim-mail' (see there).  Also, display this record
if `bbdb-completion-display-record' is non-nil,
If what has been typed is a valid completion but does not match
a unique record, display a list of completions.
If the completion is done and `bbdb-complete-mail-allow-cycling' is t
then cycle through the mails for the matching record.  If BBDB
would format a given address different from what we have in the mail buffer,
the first round of cycling reformats the address accordingly, then we cycle
through the mails for the matching record.
With prefix CYCLE-COMPLETION-BUFFER non-nil, display a list of all mails
available for cycling.

Set the variable `bbdb-complete-mail' non-nil for enabling this feature
as part of the MUA insinuation."
  (interactive (list nil current-prefix-arg))

  (bbdb-buffer) ; Make sure the database is initialized.

  ;; Completion should begin after the preceding comma (separating
  ;; two addresses) or colon (separating the header field name
  ;; from the header field body).  We want to ignore these characters
  ;; if they appear inside a quoted string (RFC 5322, Sec. 3.2.4).
  ;; Note also that a quoted string may span multiple lines
  ;; (RFC 5322, Sec. 2.2.3).
  ;; So to be save, we go back to the beginning of the header field body
  ;; (past the colon, when we are certainly not inside a quoted string),
  ;; then we parse forward, looking for commas not inside a quoted string
  ;; and positioned before END.  - This fails with an unbalanced quote.
  ;; But an unbalanced quote is bound to fail anyway.
  (when (and (not beg)
             (<= (point)
                 (save-restriction ; `mail-header-end'
                   (widen)
                   (save-excursion
                     (rfc822-goto-eoh)
                     (point)))))
    (let ((end (point))
          start pnt state)
      (save-excursion
        ;; A header field name must appear at the beginning of a line,
        ;; and it must be terminated by a colon.
        (re-search-backward "^[^ \t\n:][^:]*:[ \t\n]+")
        (setq beg (match-end 0)
              start beg)
        (goto-char beg)
        ;; If we are inside a syntactically correct header field,
        ;; all continuation lines in between the field name and point
        ;; must begin with a white space character.
        (if (re-search-forward "\n[^ \t]" end t)
            ;; An invalid header is identified via BEG set to nil.
            (setq beg nil)
          ;; Parse field body up to END
          (with-syntax-table bbdb-quoted-string-syntax-table
            (while (setq pnt (re-search-forward ",[ \t\n]*" end t))
              (setq state (parse-partial-sexp start pnt nil nil state)
                    start pnt)
              (unless (nth 3 state) (setq beg pnt))))))))

  ;; Do we have a meaningful way to set BEG if we are not in a message header?
  (unless beg
    (message "Not a valid buffer position for mail completion")
    (sit-for 1))

  (let* ((end (point))
         (done (unless beg 'nothing))
         (orig (and beg (buffer-substring beg end)))
         (completion-ignore-case t)
         (completion (and orig
                          (try-completion orig bbdb-hashtable
                                          'bbdb-completion-predicate)))
         all-completions dwim-completions one-record)

    (unless done
      ;; We get fooled if a partial COMPLETION matches "," (for example,
      ;; a comma in lf-name).  Such a partial COMPLETION cannot be protected
      ;; by quoting.  Then the comma gets interpreted as BEG.
      ;; So we never perform partial completion beyond the first comma.
      ;; This works even if we have just one record matching ORIG (thus
      ;; allowing dwim-completion) because ORIG is a substring of COMPLETION
      ;; even after COMPLETION got truncated; and ORIG by itself must be
      ;; sufficient to identify this record.
      ;; Yet if multiple records match ORIG we can only offer a *Completions*
      ;; buffer.
      (if (and (stringp completion)
               (string-match "," completion))
          (setq completion (substring completion 0 (match-beginning 0))))

      (setq all-completions (all-completions orig bbdb-hashtable
                                             'bbdb-completion-predicate))
      ;; Resolve the records matching ORIG:
      ;; Multiple completions may match the same record
      (let ((records (delete-dups
                      (apply 'append (mapcar (lambda (compl)
                                               (gethash compl bbdb-hashtable))
                                             all-completions)))))
        ;; Is there only one matching record?
        (setq one-record (and (not (cdr records))
                              (car records))))

      ;; Clean up *Completions* buffer window, if it exists
      (let ((window (get-buffer-window "*Completions*")))
        (if (window-live-p window)
            (quit-window nil window)))

      (cond
       ;; Match for a single record
       (one-record
        (let ((completion-list (if (eq t bbdb-completion-list)
                                   '(fl-name lf-name mail aka organization)
                                 bbdb-completion-list))
              (mails (bbdb-record-mail one-record))
              mail elt)
          (if (not mails)
              (progn
                (message "Matching record has no mail field")
                (sit-for 1)
                (setq done 'nothing))

            ;; Determine the mail address of ONE-RECORD to use for ADDRESS.
            ;; Do we have a preferential order for the following tests?
            ;; (1) If ORIG matches name, AKA, or organization of ONE-RECORD,
            ;;     then ADDRESS will be the first mail address of ONE-RECORD.
            (if (try-completion orig
                                (append
                                 (if (memq 'fl-name completion-list)
                                     (list (or (bbdb-record-name one-record) "")))
                                 (if (memq 'lf-name completion-list)
                                     (list (or (bbdb-record-name-lf one-record) "")))
                                 (if (memq 'aka completion-list)
                                     (bbdb-record-field one-record 'aka-all))
                                 (if (memq 'organization completion-list)
                                     (bbdb-record-organization one-record))))
                (setq mail (car mails)))
            ;; (2) If ORIG matches one or multiple mail addresses of ONE-RECORD,
            ;;     then we take the first one matching ORIG.
            ;;     We got here with MAIL nil only if `bbdb-completion-list'
            ;;     includes 'mail or 'primary.
            (unless mail
              (while (setq elt (pop mails))
                (if (try-completion orig (list elt))
                    (setq mail elt
                          mails nil))))
            ;; This error message indicates a bug!
            (unless mail (error "No match for %s" orig))

            (let ((dwim-mail (bbdb-dwim-mail one-record mail)))
              (if (string= dwim-mail orig)
                  ;; We get here if `bbdb-mail-avoid-redundancy' is 'mail-only
                  ;; and `bbdb-completion-list' includes 'mail.
                  (unless (and bbdb-complete-mail-allow-cycling
                               (< 1 (length (bbdb-record-mail one-record))))
                    (setq done 'unchanged))
                ;; Replace the text with the expansion
                (delete-region beg end)
                (insert dwim-mail)
                (bbdb-complete-mail-cleanup dwim-mail beg)
                (setq done 'unique))))))

       ;; Partial completion
       ((and (stringp completion)
             (not (bbdb-string= orig completion)))
        (delete-region beg end)
        (insert completion)
        (setq done 'partial))

       ;; Partial match not allowing further partial completion
       (completion
        (let ((completion-list (if (eq t bbdb-completion-list)
                                   '(fl-name lf-name mail aka organization)
                                 bbdb-completion-list)))
          ;; Now collect all the dwim-addresses for each completion.
          ;; Add it if the mail is part of the completions
          (dolist (key all-completions)
            (dolist (record (gethash key bbdb-hashtable))
              (let ((mails (bbdb-record-mail record))
                    accept)
                (when mails
                  (dolist (field completion-list)
                    (cond ((eq field 'fl-name)
                           (if (bbdb-string= key (bbdb-record-name record))
                               (push (car mails) accept)))
                          ((eq field 'lf-name)
                           (if (bbdb-string= key (bbdb-cache-lf-name
                                                  (bbdb-record-cache record)))
                               (push (car mails) accept)))
                          ((eq field 'aka)
                           (if (member-ignore-case key (bbdb-record-field
                                                        record 'aka-all))
                               (push (car mails) accept)))
                          ((eq field 'organization)
                           (if (member-ignore-case key (bbdb-record-organization
                                                        record))
                               (push (car mails) accept)))
                          ((eq field 'primary)
                           (if (bbdb-string= key (car mails))
                               (push (car mails) accept)))
                          ((eq field 'mail)
                           (dolist (mail mails)
                             (if (bbdb-string= key mail)
                                 (push mail accept))))))
                  (dolist (mail (delete-dups accept))
                    (push (bbdb-dwim-mail record mail) dwim-completions))))))

          (setq dwim-completions (sort (delete-dups dwim-completions)
                                       'string-lessp))
          (cond ((not dwim-completions)
                 (message "Matching record has no mail field")
                 (sit-for 1)
                 (setq done 'nothing))
                ;; DWIM-COMPLETIONS may contain only one element,
                ;; if multiple completions match the same record.
                ;; Then we may proceed with DONE set to `unique'.
                ((eq 1 (length dwim-completions))
                 (delete-region beg end)
                 (insert (car dwim-completions))
                 (bbdb-complete-mail-cleanup (car dwim-completions) beg)
                 (setq done 'unique))
                (t (setq done 'choose)))))))

    ;; By now, we have considered all possiblities to perform a completion.
    ;; If nonetheless we haven't done anything so far, consider cycling.
    ;;
    ;; Completion and cycling are really two very separate things.
    ;; Completion is controlled by the user variable `bbdb-completion-list'.
    ;; Cycling assumes that ORIG already holds a valid RFC 822 mail address.
    ;; Therefore cycling may consider different records than completion.
    (when (and (not done) bbdb-complete-mail-allow-cycling)
      ;; find the record we are working on.
      (let* ((address (bbdb-extract-address-components orig))
             (record (car (bbdb-message-search
                           (car address) (cadr address)))))
        (if (and record
                 (setq dwim-completions
                       (mapcar (lambda (m) (bbdb-dwim-mail record m))
                               (bbdb-record-mail record))))
            (cond ((and (= 1 (length dwim-completions))
                        (string= orig (car dwim-completions)))
                   (setq done 'unchanged))
                  (cycle-completion-buffer ; use completion buffer
                   (setq done 'cycle-choose))
                  ;; Reformatting / Clean up:
                  ;; If the canonical mail address (nth 1 address)
                  ;; matches the Nth canonical mail address of RECORD,
                  ;; but ORIG is not `equal' to (bbdb-dwim-mail record n),
                  ;; then we replace ORIG by (bbdb-dwim-mail record n).
                  ;; For example, the address "JOHN SMITH <FOO@BAR.COM>"
                  ;; gets reformatted as "John Smith <foo@bar.com>".
                  ;; We attempt this reformatting before the yet more
                  ;; aggressive proper cycling.
                  ((let* ((cmails (bbdb-record-mail-canon record))
                          (len (length cmails))
                          mail dwim-mail)
                     (while (and (not done)
                                 (setq mail (pop cmails)))
                       (when (and (bbdb-string= mail (nth 1 address)) ; ignore case
                                  (not (string= orig (setq dwim-mail
                                                           (nth (- len 1 (length cmails))
                                                                dwim-completions)))))
                         (delete-region beg end)
                         (insert dwim-mail)
                         (bbdb-complete-mail-cleanup dwim-mail beg)
                         (setq done 'reformat)))
                     done))

                  (t
                   ;; ORIG is `equal' to an element of DWIM-COMPLETIONS
                   ;; Use the next element of DWIM-COMPLETIONS.
                   (let ((dwim-mail (or (nth 1 (member orig dwim-completions))
                                        (nth 0 dwim-completions))))
                     ;; replace with new mail address
                     (delete-region beg end)
                     (insert dwim-mail)
                     (bbdb-complete-mail-cleanup dwim-mail beg)
                     (setq done 'cycle)))))))

    (when (member done '(choose cycle-choose))
      ;; Pop up a completions window using DWIM-COMPLETIONS.
      ;; `completion-in-region' does not work here as DWIM-COMPLETIONS
      ;; is not a collection for completion in the usual sense, but it
      ;; is really a list of replacements.
      (let ((status (not (eq (selected-window) (minibuffer-window))))
            (completion-base-position (list beg end))
            ;; We first call the default value of
            ;; `completion-list-insert-choice-function'
            ;; before performing our own stuff.
            (completion-list-insert-choice-function
             `(lambda (beg end text)
                ,(if (boundp 'completion-list-insert-choice-function)
                     `(funcall ',completion-list-insert-choice-function
                               beg end text))
                (bbdb-complete-mail-cleanup text beg))))
        (if status (message "Making completion list..."))
        (with-output-to-temp-buffer "*Completions*"
          (display-completion-list dwim-completions))
        (if status (message "Making completion list...done"))))

    ;; If DONE is `nothing' return nil so that possibly some other code
    ;; can take over.
    (unless (eq done 'nothing)
      done)))

(defun toggle-window-split ()  
  (interactive)  
  (if (= (count-windows) 2)  
      (let* ((this-win-buffer (window-buffer))  
             (next-win-buffer (window-buffer (next-window)))  
             (this-win-edges (window-edges (selected-window)))  
             (next-win-edges (window-edges (next-window)))  
             (this-win-2nd (not (and (<= (car this-win-edges)  
                                         (car next-win-edges))  
                                     (<= (cadr this-win-edges)  
                                         (cadr next-win-edges)))))  
             (splitter  
              (if (= (car this-win-edges)  
                     (car (window-edges (next-window))))  
                  'split-window-horizontally  
                'split-window-vertically)))  
        (delete-other-windows)  
        (let ((first-win (selected-window)))  
          (funcall splitter)  
          (if this-win-2nd (other-window 1))  
          (set-window-buffer (selected-window) this-win-buffer)  
          (set-window-buffer (next-window) next-win-buffer)  
          (select-window first-win)  
          (if this-win-2nd (other-window 1))))))  
