;; org html template adjustment for blog exports

;; HTML export style
(setq org-html-head-include-default-style nil) ;; use our own head styles
(setq org-html-head "<style type=\"text/css\">\n <!--/*--><![CDATA[/*><!--*/\n  .title  { text-align: center; }\n  .todo   { font-family: monospace; color: red; }\n  .done   { color: green; }\n  .tag    { background-color: #eee; font-family: monospace;\n            padding: 2px; font-size: 80%; font-weight: normal; }\n  .timestamp { color: #bebebe; }\n  .timestamp-kwd { color: #5f9ea0; }\n  .right  { margin-left: auto; margin-right: 0px;  text-align: right; }\n  .left   { margin-left: 0px;  margin-right: auto; text-align: left; }\n  .center { margin-left: auto; margin-right: auto; text-align: center; }\n  .underline { text-decoration: underline; }\n  #postamble p, #preamble p { font-size: 90%; margin: .2em; }\n  p.verse { margin-left: 3%; }\n  pre {\n    border: 1px solid #ccc;\n    box-shadow: 3px 3px 3px #eee;\n    padding: 8pt;\n    font-family: monospace;\n    overflow: auto;\n    margin: 1.2em;\n  }\n  pre.src {\n    position: relative;\n    overflow: auto;\n    padding-top: 1.2em;\n  }\n  pre.src:before {\n    display: none;\n    position: absolute;\n    background-color: white;\n    top: -10px;\n    right: 10px;\n    padding: 3px;\n    border: 1px solid black;\n  }\n  pre.src:hover:before { display: inline;}\n  pre.src-sh:before    { content: 'sh'; }\n  pre.src-bash:before  { content: 'sh'; }\n  pre.src-emacs-lisp:before { content: 'Emacs Lisp'; }\n  pre.src-R:before     { content: 'R'; }\n  pre.src-perl:before  { content: 'Perl'; }\n  pre.src-java:before  { content: 'Java'; }\n  pre.src-sql:before   { content: 'SQL'; }\n\n  table { border-collapse:collapse; }\n  caption.t-above { caption-side: top; }\n  caption.t-bottom { caption-side: bottom; }\n  td, th { vertical-align:top;  }\n  th.right  { text-align: center;  }\n  th.left   { text-align: center;   }\n  th.center { text-align: center; }\n  td.right  { text-align: right;  }\n  td.left   { text-align: left;   }\n  td.center { text-align: center; }\n  dt { font-weight: bold; }\n  .footpara:nth-child(2) { display: inline; }\n  .footpara { display: block; }\n  .footdef  { margin-bottom: 1em; }\n  .figure { padding: 1em; }\n  .figure p { text-align: center; }\n  .inlinetask {\n    padding: 10px;\n    border: 2px solid gray;\n    margin: 10px;\n    background: #ffffcc;\n  }\n  #org-div-home-and-up\n   { text-align: right; font-size: 70%; white-space: nowrap; }\n  textarea { overflow-x: auto; }\n  .linenr { font-size: smaller }\n  .code-highlighted { background-color: #ffff00; }\n  .org-info-js_info-navigation { border-style: none; }\n  #org-info-js_console-label\n    { font-size: 10px; font-weight: bold; white-space: nowrap; }\n  .org-info-js_search-highlight\n    { background-color: #ffff00; color: #000000; font-weight: bold; }\n  /*]]>*/-->\n</style>")

;; html export (for blogs)
(setq org-html-indent t
      org-html-scripts nil
      org-html-style nil)
(defun org-html--build-meta-info (info) "neutralize" (format "<!-- no meta -->\n"))

;; Exporting code blocks suitable for google code prettify (https://code.google.com/p/google-code-prettify/)
(defun org-html-src-block (src-block contents info)
  "Transcode a SRC-BLOCK element from Org to HTML.
CONTENTS holds the contents of the item.  INFO is a plist holding
contextual information.
Modified to work with google code prettify (if installed on site)"
  (if (org-export-read-attribute :attr_html src-block :textarea)
      (org-html--textarea-block src-block)
    (let ((lang (org-element-property :language src-block))
	  (caption (org-export-get-caption src-block))
	  (code (org-html-format-code src-block info))
	  (label (let ((lbl (org-element-property :name src-block)))
		   (if (not lbl) ""
		     (format " id=\"%s\""
			     (org-export-solidify-link-text lbl))))))
      (if (not lang) (format "<pre class=\"example\"%s>\n%s</pre>" label code)
	(format
	 "<div class=\"org-src-container\">\n%s%s\n</div>"
	 (if (not caption) ""
	   (format "<label class=\"org-src-name\">%s</label>"
		   (org-export-data caption info)))
	 ;; add prettyprint and lang- tags to exports
	 (format "\n<pre class=\"prettyprint lang-%s src src-%s\"%s>%s</pre>" lang lang label code))))))


;;(defun org-html-format-headline (format ""))

(defun org-html-headline (headline contents info)
  "Transcode a HEADLINE element from Org to HTML.
CONTENTS holds the contents of the headline.  INFO is a plist
holding contextual information."
  (unless (org-element-property :footnote-section-p headline)
    (let* ((contents (or contents ""))
	   (numberedp (org-export-numbered-headline-p headline info))
	   (level (org-export-get-relative-level headline info))
	   (text (org-export-data (org-element-property :title headline) info))
	   (todo (and (plist-get info :with-todo-keywords)
		      (let ((todo (org-element-property :todo-keyword headline)))
			(and todo (org-export-data todo info)))))
	   (todo-type (and todo (org-element-property :todo-type headline)))
	   (tags (and (plist-get info :with-tags)
		      (org-export-get-tags headline info)))
	   (priority (and (plist-get info :with-priority)
			  (org-element-property :priority headline)))
	   (section-number (mapconcat #'number-to-string
				      (org-export-get-headline-number
				       headline info) "-"))
	   (ids (delq 'nil
		      (list (org-element-property :CUSTOM_ID headline)
			    (concat "sec-" section-number)
			    (org-element-property :ID headline))))
	   (preferred-id (car ids))
	   (extra-ids (mapconcat
		       (lambda (id)
			 (org-html--anchor
			  (org-export-solidify-link-text
			   (if (org-uuidgen-p id) (concat "ID-" id) id))))
		       (cdr ids) ""))
	   ;; Create the headline text.
	   (full-text (org-html-format-headline--wrap headline info)))
      (if (org-export-low-level-p headline info)
	  ;; This is a deep sub-tree: export it as a list item.
	  (let* ((type (if numberedp 'ordered 'unordered))
		 (itemized-body
		  (org-html-format-list-item
		   contents type nil info nil
		   (concat (org-html--anchor preferred-id) extra-ids
			   full-text))))
	    (concat
	     (and (org-export-first-sibling-p headline info)
		  (org-html-begin-plain-list type))
	     itemized-body
	     (and (org-export-last-sibling-p headline info)
		  (org-html-end-plain-list type))))
	;; Standard headline.  Export it as a section.
	(let ((extra-class (org-element-property :HTML_CONTAINER_CLASS headline))
	      (level1 (+ level (1- org-html-toplevel-hlevel)))
	      (first-content (car (org-element-contents headline))))
	  (format "<%s id=\"%s\" class=\"%s\">%s%s</%s>\n"
		  (org-html--container headline info)
		  (format "outline-container-%s"
			  (or (org-element-property :CUSTOM_ID headline)
			      (concat "sec-" section-number)))
		  (concat (format "outline-%d" level1) (and extra-class " ")
			  extra-class)
		  (format "\n<h%d id=\"%s\">%s%s</h%d>\n"
			  level1 preferred-id extra-ids full-text level1)
		  ;; When there is no section, pretend there is an
		  ;; empty one to get the correct <div class="outline-
		  ;; ...> which is needed by `org-info.js'.
		  (if (not (eq (org-element-type first-content) 'section))
		      (concat (org-html-section first-content "" info)
			      contents)
		    contents)
		  (org-html--container headline info)))))))


(defun org-html-template (contents info)
  "Return complete document string after HTML conversion.
CONTENTS is the transcoded contents string.  INFO is a plist
holding export options."
  (concat
   ;; (when (and (not (org-html-html5-p info)) (org-html-xhtml-p info))
   ;;   (let ((decl (or (and (stringp org-html-xml-declaration)
   ;; 			      org-html-xml-declaration)
   ;; 			 (cdr (assoc (plist-get info :html-extension)
   ;; 				     org-html-xml-declaration))
   ;; 			 (cdr (assoc "html" org-html-xml-declaration))

   ;; 			 "")))
   ;;     (when (not (or (eq nil decl) (string= "" decl)))
   ;; 	 (format "%s\n"
   ;; 		 (format decl
   ;; 		  (or (and org-html-coding-system
   ;; 			   (fboundp 'coding-system-get)
   ;; 			   (coding-system-get org-html-coding-system 'mime-charset))
   ;; 		      "iso-8859-1"))))))
   ;; (org-html-doctype info)
   ;; "\n"
   ;; (concat "<html"
   ;; 	   (when (org-html-xhtml-p info)
   ;; 	     (format
   ;; 	      " xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"%s\" xml:lang=\"%s\""
   ;; 	      (plist-get info :language) (plist-get info :language)))
   ;; 	   ">\n")
   ;; "<head>\n"
   ;; (org-html--build-meta-info info)
   ;; (org-html--build-head info)
   ;; (org-html--build-mathjax-config info)
   ;; "</head>\n"
   ;; "<body>\n"
   (let ((link-up (org-trim (plist-get info :html-link-up)))
	 (link-home (org-trim (plist-get info :html-link-home))))
     (unless (and (string= link-up "") (string= link-home ""))
       (format org-html-home/up-format
	       (or link-up link-home)
	       (or link-home link-up))))
   ;; Preamble.
   ;; (org-html--build-pre/postamble 'preamble info)
   ;; Document contents.
   ;; (format "<%s id=\"%s\">\n"
   ;; 	   (nth 1 (assq 'content org-html-divs))
   ;; 	   (nth 2 (assq 'content org-html-divs)))
   ;; Document title.
   ;; (let ((title (plist-get info :title)))
   ;;   (format "<h1 class=\"title\">%s</h1>\n" (org-export-data (or title "") info)))
   contents
   ;; (format "</%s>\n"
   ;; 	   (nth 1 (assq 'content org-html-divs)))
   ;; Postamble.
   ;;(org-html--build-pre/postamble 'postamble info)
   ;; Closing document.
   ;; "</body>\n</html>"
   ))
