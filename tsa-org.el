;; ;;; tsa-org.el --- Orgmode config file

;; * Export Safely
;; This is here to allow proper agenda export without loading emacs.el
;; #+BEGIN_SRC emacs-lisp
(setq org-agenda-files
      (quote
       ("~/emacs/org/odh_agenda.org" "~/emacs/org/meals.org" "~/emacs/org/family.org" "~/emacs/org/odh.org" "~/emacs/org/agenda.org" "~/Language/Natural/conversation.org" "~/emacs/org/desiderata.org" "~/emacs/org/td.org")))


;(add-hook 'emacs-lisp-mode-hook (lambda () (make-local-variable 'orgstruct-heading-prefix-regexp) "^;;;.*"))
(setq org-catch-invisible-edits 'smart)
(require 'cl-lib)
;; #+END_SRC
;; * Images
;; #+BEGIN_SRC emacs-lisp
; Display inline org-images according to specified widths
(setq org-image-actual-width nil)
;; inline images
(defun do-org-show-all-inline-images ()
  (interactive)
  (org-display-inline-images t t))
(global-set-key (kbd "C-c C-x C-v")
                'do-org-show-all-inline-images)
;; #+end_src
;; * Exports
;; ** Org to HTML export
;; #+begin_src emacs-lisp
(setq
 ;; remove preamble
 org-html-preamble nil
 ;; remove "made with org"
 org-html-postamble nil
 ;; remove table of contents
 org-export-with-toc nil)
;; (eval-after-load "org"
;;   '(require 'ox-odt nil t))
;; enable export to ODT
;; (eval-after-load "org"
;;   '(require 'ox-md nil t)) ;; enable export to Markdown
;; (eval-after-load "org"
;;   '(require 'ox-beamer nil t))
;; enable export to Beamer Latex PDF slideshow
; (tsa/safe-load-file "/home/torysa/emacs/lisp/ox-reveal.el")
;(load "/home/torysa/emacs/lisp/ox-reveal.el") ;; Broken with Orgmode 9

;;; BROKEN with org 9.0
;; (eval-after-load "org"
;;   '(require 'ox-reveal))
;; #+end_src

;; ** Keywords and DONE states
;; #+begin_src emacs-lisp
(setq org-todo-keyword-faces
      '(("TODO" . org-warning)
	("PENDING" . "orange")
	("STARTED" . "yellow")
	("LOCALIZED" . "yellow")
	("STAGED" . "pale green")
	("PUBLISHED" . "forest green")
	("DONE" . "green")
	("HOLD" . "orange")
	("CANCELED" . (:foreground "red" :weight bold))))

(setq org-todo-keywords
      '((sequence "TODO(t)" "|" "DONE(d)")
	(sequence "PENDING(p)" "STARTED(s)" "HOLD(h)" "|" "DONE(d)")
        (sequence "|" "CANCELED(c)" "ABSTAINED(a)")
        ;(sequence "|" "ABSTAINED(a)")
        ))

(setq org-export-with-sub-superscripts '{})

;; #+end_src
;; ** Calendar exports
;; #+begin_src emacs-lisp
;; org-icalendar
(setq org-icalendar-include-todo t)
;(setenv "TZ" nil)
(setq org-icalendar-timezone "America/Denver")
(setq org-icalendar-combined-agenda-file "/home/torysa/emacs/org/cal.ics")

(add-to-list 'org-structure-template-alist '("d" ":HIDDEN:\n?\n:END:\n" ""))
;; #+end_src
;; ** Org file-apps
;; #+begin_src emacs-lisp
 ;; org open odt
(add-to-list 'org-file-apps '("\\.odt\\'" . "xdg-open %s"))
(add-to-list 'org-file-apps '("\\.mp3\\'" . "xdg-open %s"))
(add-to-list 'org-file-apps '("\\.ogv\\'" . "xdg-open %s"))
(add-to-list 'org-file-apps '("\\.mp4\\'" . "xdg-open %s"))
(setcdr (assq 'system org-file-apps-defaults-gnu ) "xdg-open %s")
;; #+end_src
;; ** Custom HTML export
;; convert =C-x= to <kbd>C-x</kbd>, *awesome* to <strong>awesome</strong>
;; #+begin_src emacs-lisp
(setq org-html-text-markup-alist   '((bold . "<strong>%s</strong>") ; *text*
    (code . "<code>%s</code>") ; ~text~
    (italic . "<i>%s</i>") ; /text/
    (strike-through . "<del>%s</del>") ; +text+
    (underline . "<span class=\"underline\">%s</span>") ; _text_
    (verbatim . "<kbd>%s</kbd>"))) ; =text=
;; #+end_src

;; ** Cleanup ODT exports
;; #+begin_src emacs-lisp
(add-hook
 'org-export-before-parsing-hook
 (lambda (backend)
   (when (eq backend 'odt)
     (goto-char (point-min))
     (when (re-search-forward
	    (rx-to-string '(one-or-more
			    (or (in (#x0 . #x8))
				(in (#xB . #xC))
				(in (#xE. #x1F))
				(in (#xD800. #xDFFF))
				(in (#xFFFE . #xFFFF))
				(in (#x110000 . #x3FFFFF))))) nil t)
       (replace-match "" t t)))))
;; #+end_src

;; * Agenda
;; ** Custom agenda commands
;; #+begin_src emacs-lisp 
(setq org-agenda-custom-commands
      '(("w"."Work")
	("wd" "Work Day"
	 ((agenda "" ((org-agenda-span 1) (org-agenda-start-on-weekday nil) (org-agenda-regexp-filter-preset '("+:ODH:\\|:AGENDA:\\|:CONFERENCE:" "-:TSA:"))))))
	("ww" "Work Week"
	 ((agenda "" ((org-agenda-span 7) (org-agenda-start-on-weekday nil) (org-agenda-regexp-filter-preset '("+:ODH:\\|:AGENDA:\\|:CONFERENCE:" "-:TSA:"))))))
	("wm" "Work Month"
	 ((agenda "" ((org-agenda-span 'month) (org-agenda-start-on-weekday nil) (org-agenda-regexp-filter-preset '("+:ODH:\\|:AGENDA:\\|:CONFERENCE:" "-:TSA:"))))))
        ("n"."Non-work")
	("nd" "Non-work Day"
	 ((agenda "" ((org-agenda-span 1) (org-agenda-start-on-weekday nil) (org-agenda-regexp-filter-preset '("-:ODH:"))))))
	("nw" "Non-work Week"
	 ((agenda "" ((org-agenda-span 7) (org-agenda-start-on-weekday nil) (org-agenda-regexp-filter-preset '("-:ODH:"))))))
	("nm" "Non-work Month"
	 ((agenda "" ((org-agenda-span 'month) (org-agenda-start-on-weekday nil) (org-agenda-regexp-filter-preset '("-:ODH:"))))))))
;; #+end_src
;; ** Agenda Variables
;; #+begin_src emacs-lisp
(setq org-agenda-diary-file "/home/torysa/emacs/org/agenda.org"
      org-special-ctrl-o nil
      org-agenda-span 'day)
(setq org-log-note-clock-out t)
;(setq org-log-note-clock-out nil)

;; Appointments
;; (setq 
;;  appt-message-warning-time 15
;;  appt-display-mode-line t
;;  appt-audible t
;;  appt-display-format 'window)
;; (appt-activate)

;; #+end_src
;; ** Agenda Holidays
;; #+begin_src emacs-lisp
;; Agenda only US holidays ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Holiday-Customizing.html
(setq holiday-bahai-holidays nil)
(setq holiday-islamic-holidays nil)
(setq holiday-hebrew-holidays nil)
(setq holiday-oriental-holidays nil)
(setq holiday-solar-holidays nil)
(setq holiday-other-holidays '((holiday-fixed 7 24 "Pioneer Day")))
;;; * Org Agenda
(setq org-agenda-sticky t)
;; #+end_src
;; ** Custom last-buffer and functions
;; #+begin_src emacs-lisp
(setq tsa/last-agenda-buffer nil)
(defun tsa/set-agenda-name ()
  (interactive)
  (message (concat "Current agenda name is " org-agenda-this-buffer-name))
  (setq tsa/last-agenda-buffer org-agenda-this-buffer-name))
(add-hook 'org-agenda-mode-hook 'tsa/set-agenda-name)

(defun tsa/make-agenda (target)
  (setq org-agenda-window-setup target)
  (org-agenda-list))

(defun tsa/go-or-make-agenda (&optional new-frame)
  (interactive "P")
  (if tsa/last-agenda-buffer
      (if new-frame
          (switch-to-buffer-other-frame tsa/last-agenda-buffer)
        (switch-to-buffer tsa/last-agenda-buffer))
    (if new-frame
        (tsa/make-agenda 'other-frame)
      (tsa/make-agenda 'current-window)))
  (cd org-directory))
;; #+end_src
;; ** Agenda Appearance
;; #+begin_src emacs-lisp
(setq org-agenda-day-face-function nil)
 ;; Diary schedule
(defun diary-schedule (m1 d1 y1 m2 d2 y2 dayname)
  "Entry applies if date is between dates on DAYNAME.  
    Order of the parameters is M1, D1, Y1, M2, D2, Y2 if
    `european-calendar-style' is nil, and D1, M1, Y1, D2, M2, Y2 if
    `european-calendar-style' is t. Entry does not apply on a history."
  (let ((date1 (calendar-absolute-from-gregorian
		(if european-calendar-style
		    (list d1 m1 y1)
		  (list m1 d1 y1))))
	(date2 (calendar-absolute-from-gregorian
		(if european-calendar-style
		    (list d2 m2 y2)
		  (list m2 d2 y2))))
	(d (calendar-absolute-from-gregorian date)))
    (if (and 
	 (<= date1 d) 
	 (<= d date2)
	 (= (calendar-day-of-week date) dayname)
	 (not (check-calendar-holidays date)))
	entry)))
;; #+end_src
;; * Misc
;; ** org time format
;; #+begin_src emacs-lisp
(setq org-time-clocksum-format '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))

;; #+end_src
;; ** For Org mode (task management)
;; #+begin_src emacs-lisp
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-agenda-include-diary nil)
(fset 'tsa/org-agenda-add-today
      (lambda (&optional arg) "Keyboard macro." (interactive "p")
        (kmacro-exec-ring-item (quote ([return 3 16 14 5 return 21 3 46 return 24 98 return] 0 "%d")) arg)
        (message "Gave event \"TODAY\" date")))
(with-eval-after-load 'org-agenda (define-key org-agenda-mode-map (kbd "C-c .") 'tsa/org-agenda-add-today))
(with-eval-after-load 'org-agenda (define-key org-agenda-mode-map (kbd "C-c C-.") 'tsa/org-agenda-add-today))
;; #+end_src

;; ** org-contacts for gnus
;; #+begin_src emacs-lisp
;(tsa/safe-load-file "/home/torysa/.emacs.d/lisp/org-contacts.el")
;; (load "/home/torysa/.emacs.d/lisp/org-contacts.el")
;; (require 'org-contacts)
;; (add-to-list 'org-contacts-files "/home/torysa/emacs/org/contacts.org")

;;(require 'org-trello)
;; #+end_src
;; ** ebib
;; #+begin_src emacs-lisp
(org-add-link-type "ebib" 'ebib-open-org-link)
;; #+end_src
;; ** Struct mode (for messages)
;; #+begin_src emacs-lisp
;; Org struct mode
(add-hook 'message-mode-hook 'turn-on-orgstruct++)
(add-hook 'emacs-lisp-mode-hook 'turn-on-orgstruct++)

;; #+end_src
;; ** Clock-in
;; #+begin_src emacs-lisp
(org-clock-persistence-insinuate)
(setq org-clock-persist t
      org-clock-persist-query-resume nil)
;;; * Orgmode Modules
(add-to-list 'org-modules 'habits)
;; Number of clock tasks to remember in history.
(setq org-clock-history-length 35)  ; 1 to 9 + A to Z
;; #+end_src
;; ** mail link
;; #+begin_src emacs-lisp

(setq org-email-link-description-format "Email %c: %s") ; give full email subject
 ;; saving views

 ;;; * bmkp links http://orgmode.org/manual/Adding-hyperlink-types.html
     (org-add-link-type "bmkp" 'org-bmkp-open)
     (add-hook 'org-store-link-functions 'org-bmkp-store-link)
;; #+end_src
;; ** Bookmarks
;; #+begin_src emacs-lisp
(defcustom org-bmkp-command 'bookmark-jump
  "The Emacs command to be used to display a bmkp page."
  :group 'org-link
  :type '(choice (const bookmark-jump) (const bookmark-jump-other-window)))

(defun org-bmkp-open (path)
  "Visit the bmkppage on PATH.
     PATH should be a bookmark name that can be thrown at the `bookmark-jump' function."
  (funcall org-bmkp-command path))

(defun org-bmkp-store-link ()
  "Store a link to a bmkp bookmark."
  (when (memq major-mode '(bookmark-bmenu-mode))
    (let* ((bookmark (bookmark-bmenu-bookmark))
	   (link (concat "bmkp:" bookmark))
	   (description (format "Bookmark: %s" bookmark)))
      (org-store-link-props
       :type "bmkp"
       :link link
       :description description))))

     ;(provide 'org-bmkp)

(setq appt-display-format 'window)
(define-key org-mode-map (kbd "C-c SPC") 'ace-jump-mode)
;(define-key org-mode-map (kbd "C-c C-s") 'org-schedule)
(define-key org-mode-map (kbd "C-c s") 'helm-org-in-buffer-headings)
(setq org-footnote-define-inline nil) ; Create footnotes with abbrevs

;; #+end_src
;; ** Orgmode timers
;; #+begin_src emacs-lisp
(defun org-notify-display-appt (min-to-app new-time appt-msg)
  "Use `org-notify' to display an appointment.
  You can use this for `appt-disp-window-function'.
  `appt-display-format' should be set to 'window, and
  `appt-delete-window-function' should be a function that does
  nothing."
  ;; FIXME: Update the message to incorporate MIN-TO-APP.  See
  ;; `appt-disp-window'.
  (org-notify appt-msg))

(setq appt-disp-window-function #'org-notify-display-appt)
(setq appt-delete-window-function (lambda nil))
(setq org-agenda-skip-deadline-prewarning-if-scheduled t) ; Use schedules instead of deadline pre-warnings
(setq org-agenda-default-appointment-duration 55) ; How long are default appointments?
(setq org-src-fontify-natively t) ;; fontify src blogs
;(tsa/safe-load-file "/home/torysa/.emacs.d/lisp/tsa-org-html.el")
;; #+end_src
;; ** org reveal
;; #+begin_src emacs-lisp
 ;; Org Reveal
;(setq org-reveal-root "file://home/torysa/Workspace/JavaScript/reveal.js/js/reveal.js")
(setq org-reveal-root "file:///home/torysa/Workspace/JavaScript/reveal.js")
;; #+END_SRC
;; ** Capture
;; #+BEGIN_SRC emacs-lisp
(setq org-default-notes-file (concat org-directory "/home/torysa/emacs/org/td.org"))

(setq org-capture-templates
      (append '(("i" "Icebox" entry (file+datetree "/home/torysa/emacs/org/icebox.org")
               "* TODO %U %^{prompt}
%?" :prepend t)
		("l" "Cool Learning" entry (file "/home/torysa/emacs/org/coollearning.org")
		 "* %U %^{prompt}
                  %?" :prepend t)
		("t" "Add pending tasks")
		("tt" "Today TODO" entry (file+headline "/home/torysa/emacs/org/td.org" "Today")
		 "* TODO %^{prompt}\n\tDEADLINE: %T\n%?" :prepend t)
		("tT" "Future TODO" entry (file+headline "/home/torysa/emacs/org/td.org" "Tomorrow")
		 "* TODO %^{prompt}\n\tDEADLINE: %^{Deadline}T%?" :prepend t)
		("to" "ODH Today TODO" entry (file+headline "/home/torysa/emacs/org/odh_agenda.org" "TODAY TODO")
                 "* TODO %^{prompt}\n\tDEADLINE: %T\n%?" :prepend t)
		("tO" "ODH Future TODO" entry (file+headline "/home/torysa/emacs/org/odh_agenda.org" "MISC TODO")
		 "* TODO %^{prompt}\n\tDEADLINE: %^{Deadline}T%? \n %T" :prepend t)
		("te" "Event from Email" entry (file+headline org-agenda-diary-file "TODAY TODO")
		 "* %A\n\t%^T\n%?" :prepend t)
                ("tE" "Personal Task from Email" entry (file+headline "/home/torysa/emacs/org/agenda.org" "TODAY TODO")
                 "* TODO %A\nDEADLINE: %T\n%?" :prepend t)
                ("th" "ODH Task from Email" entry (file+headline "/home/torysa/emacs/org/odh_agenda.org" "TODAY TODO")
                 "* TODO %A\nDEADLINE: %T\n%?" :prepend t)
                ("m" "Schedule Meal" entry (file+datetree+prompt "~/emacs/org/meals.org")
		 "* TODO %^{prompt}
		%^T%?" :prepend t)
                ("pt" "PhD Task" entry (file+headline "/home/torysa/emacs/org/phd.org" "Worklist")
		 "* TODO %^{prompt}\n\tDEADLINE: %T\n%?" :prepend t)
                ("pE" "PhD Task from Email" entry (file+headline "/home/torysa/emacs/org/phd.org" "Worklist")
                 "* TODO %A\nDEADLINE: %T\n%?" :prepend t)
		)))
;; #+END_SRC

