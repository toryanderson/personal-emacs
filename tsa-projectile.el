;; Projectile & Helm Projectile settings
(require 'projectile)
(require 'helm-projectile)
(projectile-global-mode)
(setq projectile-completion-system 'helm)
;(helm-projectile-on) ;; no longer necessary? 
;(setq projectile-switch-project-action 'helm-projectile-find-file)
(setq projectile-switch-project-action 'helm-projectile)

;; Helm Projectile
(define-key projectile-command-map (kbd "s g") 'helm-projectile-grep)
