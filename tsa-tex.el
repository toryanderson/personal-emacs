;; My Tex Setup
;; Auctex: produce PDF by default.
(setq TeX-PDF-mode t
      TeX-global-PDF-mode t
      TeX-auto-save t
      TeX-parse-self t)


					;(setq TeX-view-program-selection '((output-pdf "Okular")))
(setq TeX-view-program-list '(("Emacs" "emacsclient %o")))
(setq TeX-view-program-selection '((output-pdf "Emacs")))

;; fix hunspell with Latex quotes
(defadvice ispell-send-string (before kill-quotes activate)
  (setq string (replace-regexp-in-string "''" "  " string)))

;; AucTex LaTeX
(setq LaTeX-command-style '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)")))

;; (add-hook 'latex-mode-hook 'flyspell-mode)
;(add-hook 'latex-mode-hook 'ac-ispell-ac-setup)
